name := "shediac"

version := "1.0"
organization := "ca.uwaterloo.shediac"
//scalaVersion := "2.11.7"
//javacOptions ++= Seq("-source", "1.8", "-target", "1.8")
javaOptions += "-Xmx8g"

javacOptions in (Compile, compile) ++= Seq("-source", "1.8", "-target", "1.8", "-g:lines")

crossPaths := false
autoScalaLibrary := false

unmanagedJars in Compile += file("lib/jcuda/jcuda-0.7.5.jar")
unmanagedJars in Compile += file("lib/jocl/JOCL-0.1.9.jar")
