mvn install:install-file -Dfile=lib/jocl/JOCL-0.1.9.jar -DgroupId=org.jocl -DartifactId=jocl -Dversion=0.1.9 -Dpackaging=jar
mvn install:install-file -Dfile=lib/jcuda/jcuda-0.7.5.jar -DgroupId=jcuda -DartifactId=jcuda -Dversion=0.7.5 -Dpackaging=jar

To build: mvn package
To run: java -Djava.library.path=lib/jocl:lib/jcuda -cp target/shediac-1.0-SNAPSHOT.jar:lib/jocl/JOCL-0.1.9.jar:lib/jcuda/jcuda-0.7.5.jar full_package_name_of_class_with_main_method
