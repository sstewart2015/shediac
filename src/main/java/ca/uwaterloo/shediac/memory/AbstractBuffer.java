package ca.uwaterloo.shediac.memory;

public abstract class AbstractBuffer implements Buffer {

  protected final BufferAccess access;

  public AbstractBuffer(final BufferAccess a) {
    access = a;
  }

}
