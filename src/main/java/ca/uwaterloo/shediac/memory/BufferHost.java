package ca.uwaterloo.shediac.memory;

import ca.uwaterloo.shediac.constant.Constant;

public interface BufferHost {

  int getSize();

  void release();

  void set(int idx, Constant val);

  Constant get(int idx, Type type);

}
