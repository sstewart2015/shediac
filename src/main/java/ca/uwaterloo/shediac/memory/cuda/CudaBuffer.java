package ca.uwaterloo.shediac.memory.cuda;

import jcuda.Pointer;
import jcuda.driver.CUdeviceptr;
import jcuda.driver.CUstream;
import jcuda.driver.JCudaDriver;
import ca.uwaterloo.shediac.constant.ByteConstant;
import ca.uwaterloo.shediac.constant.Constant;
import ca.uwaterloo.shediac.constant.DoubleConstant;
import ca.uwaterloo.shediac.constant.FloatConstant;
import ca.uwaterloo.shediac.constant.IntConstant;
import ca.uwaterloo.shediac.device.Queue;
import ca.uwaterloo.shediac.device.cuda.CudaQueue;
import ca.uwaterloo.shediac.memory.AbstractBuffer;
import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.BufferConfig;
import ca.uwaterloo.shediac.memory.BufferHost;
import ca.uwaterloo.shediac.memory.Type;

public class CudaBuffer extends AbstractBuffer {

  public final CUdeviceptr ptr;

  public CudaBuffer(final CUdeviceptr ptr) {
    super(BufferAccess.NotOwned);
    this.ptr = ptr;
  }

  public CudaBuffer(final CUdeviceptr ptr, final BufferAccess access) {
    super(access);
    this.ptr = ptr;
  }

  public CudaBuffer(final BufferConfig config) {
    super(config.access);
    ptr = new CUdeviceptr();
    JCudaDriver.cuMemAlloc(ptr, config.size);
  }

  public CudaBuffer(final long size) {
    super(BufferAccess.ReadWrite); // default access
    ptr = new CUdeviceptr();
    JCudaDriver.cuMemAlloc(ptr, size);
  }


  @Override
  public Constant get(final Queue q, final int idx, final Type type) {
    switch (type) {
      case BYTE:
        return getByte(q, idx);
      case DOUBLE:
        return getDouble(q, idx);
      case FLOAT:
        return getFloat(q, idx);
      case INTEGER:
        return getInt(q, idx);
      default:
        throw new RuntimeException("Invalid or unsupported data type");
    }
  }

  @Override
  public void set(final Queue queue, final int idx, final Constant val) {
    if (val instanceof IntConstant) {
      final int n = (Integer) ((IntConstant) val).asJavaVal();
      setInt(queue, idx, n);
    } else if (val instanceof DoubleConstant) {
      final double n = (Double) ((DoubleConstant) val).asJavaVal();
      setDouble(queue, idx, n);
    } else {
      throw new RuntimeException("Invalid or unsupported data type");
    }
  }

  @Override
  public void readAsync(Queue q, long size, BufferHost hostPtr, long offset) {
    if (access.equals(BufferAccess.WriteOnly))
      throw new jcuda.CudaException("reading from a write-only buffer");
    Pointer host = ((CudaBufferHost) hostPtr).ptr.getPointerCu().withByteOffset(offset);
    CUstream stream = ((CudaQueue) q).queue;
    JCudaDriver.cuMemcpyDtoHAsync(host, ptr, size, stream);
  }

  @Override
  public void read(Queue q, long size, BufferHost hostPtr, long offset) {
    readAsync(q, size, hostPtr, offset);
    q.finish();
  }

  @Override
  public void writeAsync(Queue q, long size, BufferHost hostPtr, long offset) {
    if (access.equals(BufferAccess.ReadOnly))
      throw new jcuda.CudaException("Writing to a read-only buffer.");
    if (getSize() < (offset + size))
      throw new jcuda.CudaException("Target device buffer is too small.");
    Pointer host = ((CudaBufferHost) hostPtr).ptr.getPointerCu().withByteOffset(offset);
    CUstream stream = ((CudaQueue) q).queue;
    JCudaDriver.cuMemcpyHtoDAsync(ptr, host, size, stream);
  }

  @Override
  public void write(Queue q, long size, BufferHost hostPtr, long offset) {
    writeAsync(q, size, hostPtr, offset);
    q.finish();
  }

  @Override
  public void copyToAsync(Queue q, long size, Buffer dst) {
    CUstream stream = ((CudaQueue) q).queue;
    CUdeviceptr destination = ((CudaBuffer) dst).ptr;
    JCudaDriver.cuMemcpyDtoDAsync(destination, ptr, size, stream);
  }

  @Override
  public void copyTo(Queue q, long size, Buffer dst) {
    copyToAsync(q, size, dst);
    q.finish();
  }

  @Override
  public int getSize() {
    long[] result = new long[1];
    JCudaDriver.cuMemGetAddressRange(null, result, ptr);
    return (int) result[0];
  }

  @Override
  public void release() {
    JCudaDriver.cuMemFree(ptr);
  }

  private DoubleConstant getDouble(final Queue q, final int idx) {
    double[] result = new double[1];
    JCudaDriver.cuMemcpyDtoH(Pointer.to(result), ptr.withByteOffset(idx * Double.BYTES),
        Double.BYTES);
    return new DoubleConstant(result[0]);
  }

  private FloatConstant getFloat(final Queue q, final int idx) {
    float[] result = new float[1];
    JCudaDriver
        .cuMemcpyDtoH(Pointer.to(result), ptr.withByteOffset(idx * Float.BYTES), Float.BYTES);
    return new FloatConstant(result[0]);
  }

  private IntConstant getInt(final Queue q, final int idx) {
    int[] result = new int[1];
    JCudaDriver.cuMemcpyDtoH(Pointer.to(result), ptr.withByteOffset(idx * Integer.BYTES),
        Integer.BYTES);
    return new IntConstant(result[0]);
  }

  private ByteConstant getByte(Queue q, int idx) {
    byte[] result = new byte[1];
    JCudaDriver.cuMemcpyDtoH(Pointer.to(result), ptr.withByteOffset(idx * Byte.BYTES), Byte.BYTES);
    return new ByteConstant(result[0]);
  }

  private void setDouble(final Queue q, final int idx, final double val) {
    JCudaDriver.cuMemcpyHtoD(ptr.withByteOffset(idx * Double.BYTES),
        Pointer.to(new double[] {val}), Double.BYTES);
  }

  private void setInt(final Queue q, final int idx, final int val) {
    JCudaDriver.cuMemcpyHtoD(ptr.withByteOffset(idx * Integer.BYTES), Pointer.to(new int[] {val}),
        Integer.BYTES);
  }

  @Override
  public BufferAccess getAccess() {
    return access;
  }

  @Override
  public Buffer withByteOffset(final long offset) {
    return new CudaBuffer(ptr.withByteOffset(offset), this.access);
  }

}
