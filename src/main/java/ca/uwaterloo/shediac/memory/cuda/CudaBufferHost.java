package ca.uwaterloo.shediac.memory.cuda;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.memory.AbstractBufferHost;
import ca.uwaterloo.shediac.memory.Pointer;
import jcuda.driver.JCudaDriver;

public class CudaBufferHost extends AbstractBufferHost {

  final Pointer ptr;
  
  public CudaBufferHost(final int size, final boolean pinned) {
    super(size, pinned);
    if (pinned) {
      ptr = new Pointer(DeviceType.CUDA);
      JCudaDriver.cuMemAllocHost(ptr.getPointerCu(), size);
    } else {
      buf = ByteBuffer.allocateDirect(size).order(ByteOrder.nativeOrder());
      ptr = Pointer.to(buf);
    }
    buf = ptr.getPointerCu().getByteBuffer(0L, (long) size);
  }
  
  public CudaBufferHost(final Pointer p) {
    super(p.getSize(), false);
    ptr = p;
  }

  public CudaBufferHost(final int[] data) {
    super(data.length * Integer.BYTES, false);
    ptr = Pointer.to(data);
  }

  public CudaBufferHost(final float[] data) {
    super(data.length * Float.BYTES, false);
    ptr = Pointer.to(data);
  }

  public CudaBufferHost(final double[] data) {
    super(data.length * Double.BYTES, false);
    ptr = Pointer.to(data);
  }

  public CudaBufferHost(final char[] data) {
    super(data.length * Character.BYTES, false);
    ptr = Pointer.to(data);
  }

  public CudaBufferHost(final byte[] data) {
    super(data.length * Byte.BYTES, false);
    ptr = Pointer.to(data);
  }

  public CudaBufferHost(final long[] data) {
    super(data.length * Long.BYTES, false);
    ptr = Pointer.to(data);
  }

  public CudaBufferHost(final short[] data) {
    super(data.length * Short.BYTES, false);
    ptr = Pointer.to(data);
  }

  @Override
  public void release() {
    if (pinned) {
      JCudaDriver.cuMemFreeHost(ptr.getPointerCu());
    }
  }

}
