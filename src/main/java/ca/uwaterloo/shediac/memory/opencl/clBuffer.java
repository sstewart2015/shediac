package ca.uwaterloo.shediac.memory.opencl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.jocl.CL;
import org.jocl.Pointer;
import org.jocl.cl_buffer_region;
import org.jocl.cl_command_queue;
import org.jocl.cl_mem;

import ca.uwaterloo.shediac.constant.ByteConstant;
import ca.uwaterloo.shediac.constant.Constant;
import ca.uwaterloo.shediac.constant.DoubleConstant;
import ca.uwaterloo.shediac.constant.FloatConstant;
import ca.uwaterloo.shediac.constant.IntConstant;
import ca.uwaterloo.shediac.device.Queue;
import ca.uwaterloo.shediac.device.opencl.clContext;
import ca.uwaterloo.shediac.device.opencl.clError;
import ca.uwaterloo.shediac.device.opencl.clQueue;
import ca.uwaterloo.shediac.memory.AbstractBuffer;
import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.BufferConfig;
import ca.uwaterloo.shediac.memory.BufferHost;
import ca.uwaterloo.shediac.memory.Type;

public class clBuffer extends AbstractBuffer {

  public final cl_mem buf;

  public clBuffer(final cl_mem buf) {
    super(BufferAccess.NotOwned);
    this.buf = buf;
  }

  public clBuffer(final cl_mem buf, final BufferAccess access) {
    super(access);
    this.buf = buf;
  }

  public clBuffer(final clContext c, final BufferConfig config) {
    super(config.access);
    long flags = 0L;
    if (access == BufferAccess.ReadWrite)
      flags |= CL.CL_MEM_READ_WRITE;
    else if (access == BufferAccess.ReadOnly)
      flags |= CL.CL_MEM_READ_ONLY;
    else if (access == BufferAccess.WriteOnly)
      flags |= CL.CL_MEM_WRITE_ONLY;
    else
      throw new RuntimeException();
    int[] err = new int[1];
    buf = CL.clCreateBuffer(c.context, flags, config.size, null, err);
    clError.checkError(err);
  }

  @Override
  public void readAsync(Queue q, long size, BufferHost hostPtr, long offset) {
    if (access.equals(BufferAccess.WriteOnly))
      throw new org.jocl.CLException("reading from a write-only buffer");
    Pointer host = ((clBufferHost) hostPtr).getPointer();
    cl_command_queue queue = ((clQueue) q).queue;
    CL.clEnqueueReadBuffer(queue, buf, CL.CL_FALSE, offset, size, host, 0, null, null);
  }

  @Override
  public void read(Queue q, long size, BufferHost hostPtr, long offset) {
    if (access.equals(BufferAccess.WriteOnly))
      throw new org.jocl.CLException("reading from a write-only buffer");
    Pointer host = ((clBufferHost) hostPtr).getPointer();
    cl_command_queue queue = ((clQueue) q).queue;
    CL.clEnqueueReadBuffer(queue, buf, CL.CL_TRUE, offset, size, host, 0, null, null);
  }

  @Override
  public void writeAsync(Queue q, long size, BufferHost hostPtr, long offset) {
    if (access.equals(BufferAccess.ReadOnly))
      throw new org.jocl.CLException("writing to a read-only buffer");
    if (getSize() < (offset + size))
      throw new org.jocl.CLException("Target device buffer is too small.");
    Pointer host = ((clBufferHost) hostPtr).getPointer();
    cl_command_queue queue = ((clQueue) q).queue;
    CL.clEnqueueWriteBuffer(queue, buf, CL.CL_FALSE, offset, size, host, 0, null, null);
  }

  @Override
  public void write(Queue q, long size, BufferHost hostPtr, long offset) {
    if (access.equals(BufferAccess.ReadOnly))
      throw new org.jocl.CLException("writing to a read-only buffer");
    if (getSize() < (offset + size))
      throw new org.jocl.CLException("Target device buffer is too small.");
    Pointer host = ((clBufferHost) hostPtr).getPointer();
    cl_command_queue queue = ((clQueue) q).queue;
    CL.clEnqueueWriteBuffer(queue, buf, CL.CL_TRUE, offset, size, host, 0, null, null);
  }

  @Override
  public void copyToAsync(Queue q, long size, Buffer dst) {
    cl_command_queue queue = ((clQueue) q).queue;
    CL.clEnqueueCopyBuffer(queue, buf, ((clBuffer) dst).buf, 0, 0, size, 0, null, null);
  }

  @Override
  public void copyTo(Queue q, long size, Buffer dst) {
    copyToAsync(q, size, dst);
    q.finish();
  }

  @Override
  public int getSize() {
    long[] bytes = new long[1];
    CL.clGetMemObjectInfo(buf, CL.CL_MEM_SIZE, 0, null, bytes);
    long[] result = new long[1];
    CL.clGetMemObjectInfo(buf, CL.CL_MEM_SIZE, bytes[0], Pointer.to(result), null);
    return (int) result[0];
  }

  @Override
  public void release() {
    clError.checkError(CL.clReleaseMemObject(buf));
  }

  @Override
  public Constant get(Queue q, int idx, Type type) {
    switch (type) {
      case BYTE:
        return getByte(q, idx);
      case DOUBLE:
        return getDouble(q, idx);
      case FLOAT:
        return getFloat(q, idx);
      case INTEGER:
        return getInt(q, idx);
      default:
        throw new RuntimeException("Invalid or unsupported data type");
    }
  }

  @Override
  public void set(Queue queue, int idx, Constant val) {
    if (val instanceof IntConstant) {
      final int n = (Integer) ((IntConstant) val).asJavaVal();
      setInt(queue, idx, n);
    } else if (val instanceof DoubleConstant) {
      final double n = (Double) ((DoubleConstant) val).asJavaVal();
      setDouble(queue, idx, n);
    } else if (val instanceof FloatConstant) {
      final float n = (Float) ((FloatConstant) val).asJavaVal();
      setFloat(queue, idx, n);
    } else if (val instanceof ByteConstant) {
      final byte n = (Byte) ((ByteConstant) val).asJavaVal();
      setByte(queue, idx, n);
    } else {
      throw new RuntimeException("Invalid or unsupported data type");
    }
  }

  private void setValue(final clQueue q, final int idx, final Pointer val, final long offset,
      final int size) {
    if (access.equals(BufferAccess.ReadOnly))
      throw new org.jocl.CLException("writing to a read-only buffer");
    CL.clEnqueueWriteBuffer(q.queue, buf, CL.CL_TRUE, offset, size, Pointer.to(val), 0, null, null);
  }

  private byte[] getValue(final clQueue q, final int idx, final long offset, final int size) {
    if (access.equals(BufferAccess.WriteOnly))
      throw new org.jocl.CLException("reading from a write-only buffer");
    final byte[] val = new byte[size];
    CL.clEnqueueReadBuffer(q.queue, buf, CL.CL_TRUE, offset, size, Pointer.to(val), 0, null, null);
    return val;
  }

  private void setInt(final Queue q, final int idx, final int val) {
    setValue((clQueue) q, idx, Pointer.to(new int[] {val}), idx * Integer.BYTES, Integer.BYTES);
  }

  private void setFloat(final Queue q, final int idx, final float val) {
    setValue((clQueue) q, idx, Pointer.to(new float[] {val}), idx * Float.BYTES, Float.BYTES);
  }

  private void setDouble(final Queue q, final int idx, final double val) {
    setValue((clQueue) q, idx, Pointer.to(new double[] {val}), idx * Double.BYTES, Double.BYTES);
  }

  private void setByte(final Queue q, final int idx, final byte val) {
    setValue((clQueue) q, idx, Pointer.to(new byte[] {val}), idx * Byte.BYTES, Byte.BYTES);
  }

  private IntConstant getInt(final Queue q, final int idx) {
    byte[] val = getValue((clQueue) q, idx, idx * Integer.BYTES, Integer.BYTES);
    return new IntConstant(ByteBuffer.wrap(val).order(ByteOrder.nativeOrder()).getInt());
  }

  private FloatConstant getFloat(final Queue q, final int idx) {
    byte[] val = getValue((clQueue) q, idx, idx * Integer.BYTES, Float.BYTES);
    return new FloatConstant(ByteBuffer.wrap(val).order(ByteOrder.nativeOrder()).getFloat());
  }

  private DoubleConstant getDouble(final Queue q, final int idx) {
    byte[] val = getValue((clQueue) q, idx, idx * Double.BYTES, Double.BYTES);
    return new DoubleConstant(ByteBuffer.wrap(val).order(ByteOrder.nativeOrder()).getDouble());
  }

  private ByteConstant getByte(final Queue q, final int idx) {
    byte[] val = getValue((clQueue) q, idx, idx * Double.BYTES, Double.BYTES);
    return new ByteConstant(val[idx]);
  }

  @Override
  public BufferAccess getAccess() {
    return access;
  }

  @Override
  public Buffer withByteOffset(final long offset) {
    int[] err = new int[1];
    org.jocl.cl_buffer_region region = new cl_buffer_region(offset, getSize() - offset);
    clBuffer subBuffer =
        new clBuffer(CL.clCreateSubBuffer(buf, flags(access), CL.CL_BUFFER_CREATE_TYPE_REGION,
            region, err));
    clError.checkError(err);
    return subBuffer;
  }

  private long flags(final BufferAccess access) {
    long flags = 0L;
    if (access == BufferAccess.ReadWrite)
      flags |= CL.CL_MEM_READ_WRITE;
    else if (access == BufferAccess.ReadOnly)
      flags |= CL.CL_MEM_READ_ONLY;
    else if (access == BufferAccess.WriteOnly)
      flags |= CL.CL_MEM_WRITE_ONLY;
    else
      throw new RuntimeException();
    return flags;
  }
}
