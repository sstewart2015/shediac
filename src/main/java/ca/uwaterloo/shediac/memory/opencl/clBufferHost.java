package ca.uwaterloo.shediac.memory.opencl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.memory.AbstractBufferHost;
import ca.uwaterloo.shediac.memory.Pointer;

public class clBufferHost extends AbstractBufferHost {

  final Pointer ptr;

  public clBufferHost(final int size, final boolean pinned) {
    super(size, pinned);
    if (pinned) {
      ptr = new Pointer(DeviceType.OpenCL);
      // TODO Pinned host memory for OpenCL
      throw new UnsupportedOperationException();
    } else {
      buf = ByteBuffer.allocateDirect(size).order(ByteOrder.nativeOrder());
      ptr = Pointer.to(buf);
    }
  }
  
  public clBufferHost(final Pointer p) {
    super(p.getSize(), false);
    ptr = p;
  }
  
  public clBufferHost(final int[] data) {
    super(data.length * Integer.BYTES, false);
    ptr = Pointer.to(DeviceType.OpenCL, data);
  }

  public clBufferHost(final float[] data) {
    super(data.length * Float.BYTES, false);
    ptr = Pointer.to(DeviceType.OpenCL, data);
  }

  public clBufferHost(final double[] data) {
    super(data.length * Double.BYTES, false);
    ptr = Pointer.to(DeviceType.OpenCL, data);
  }

  public clBufferHost(final char[] data) {
    super(data.length * Character.BYTES, false);
    ptr = Pointer.to(DeviceType.OpenCL, data);
  }

  public clBufferHost(final byte[] data) {
    super(data.length * Byte.BYTES, false);
    ptr = Pointer.to(DeviceType.OpenCL, data);
  }

  public clBufferHost(final long[] data) {
    super(data.length * Long.BYTES, false);
    ptr = Pointer.to(DeviceType.OpenCL, data);
  }

  public clBufferHost(final short[] data) {
    super(data.length * Short.BYTES, false);
    ptr = Pointer.to(DeviceType.OpenCL, data);
  }

  @Override
  public void release() {
    // TODO Auto-generated method stub

  }

  public org.jocl.Pointer getPointer() {
    return ptr.getPointerCl();
  }

}
