package ca.uwaterloo.shediac.memory;

import ca.uwaterloo.shediac.memory.Buffer.BufferAccess;

public final class BufferConfig {

  public int size;
  public BufferAccess access;
  public boolean pinned; // page-locked host memory
  
  public BufferConfig(final int size, final BufferAccess access) {
    this.size = size;
    this.access = access;
  }
}
