package ca.uwaterloo.shediac.memory;

import ca.uwaterloo.shediac.constant.Constant;
import ca.uwaterloo.shediac.device.Queue;

public interface Buffer {

  enum BufferAccess {
    ReadOnly, WriteOnly, ReadWrite, NotOwned
  }

  void readAsync(Queue q, long size, BufferHost hostPtr, long offset);

  /**
   * Read.
   * 
   * @param q
   * @param size
   * @param hostPtr
   * @param offset
   */
  void read(Queue q, long size, BufferHost hostPtr, long offset);

  void writeAsync(Queue q, long size, BufferHost hostPtr, long offset);

  void write(Queue q, long size, BufferHost hostPtr, long offset);

  void copyToAsync(Queue q, long size, Buffer dst);

  void copyTo(Queue q, long size, Buffer dst);

  int getSize();

  void release();

  Constant get(Queue q, int idx, final Type type);

  void set(Queue queue, int idx, Constant val);

  BufferAccess getAccess();

  Buffer withByteOffset(long offset);

}
