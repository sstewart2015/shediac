package ca.uwaterloo.shediac.memory;

public enum Type {
  SHORT, INTEGER, LONG, FLOAT, DOUBLE, BOOLEAN, CHAR, STRING, BYTE;

  public int size;

  static {
    SHORT.size = Short.BYTES;
    INTEGER.size = Integer.BYTES;
    LONG.size = Long.BYTES;
    FLOAT.size = Float.BYTES;
    DOUBLE.size = Double.BYTES;
    BOOLEAN.size = 1; // TODO Investigate
    CHAR.size = Character.BYTES;
    BYTE.size = Byte.BYTES;
  }

  public static <T> Type decode(T val) {
    if (val instanceof Integer)
      return Type.INTEGER;
    else if (val instanceof Float)
      return Type.FLOAT;
    else if (val instanceof Double)
      return Type.DOUBLE;
    else if (val instanceof Short)
      return Type.SHORT;
    else if (val instanceof Boolean)
      return Type.BOOLEAN;
    else if (val instanceof Character)
      return Type.CHAR;
    else if (val instanceof String)
      return Type.STRING;
    else if (val instanceof Byte)
      return Type.BYTE;
    throw new IllegalArgumentException();
  }

  public static boolean isNumerical(Type type) {
    return (type.equals(Type.SHORT) || type.equals(Type.INTEGER)
        || type.equals(Type.LONG) || type.equals(Type.FLOAT) || type
          .equals(Type.DOUBLE));
  }
}
