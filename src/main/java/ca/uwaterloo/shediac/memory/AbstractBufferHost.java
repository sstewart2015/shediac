package ca.uwaterloo.shediac.memory;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import ca.uwaterloo.shediac.constant.ByteConstant;
import ca.uwaterloo.shediac.constant.Constant;
import ca.uwaterloo.shediac.constant.DoubleConstant;
import ca.uwaterloo.shediac.constant.FloatConstant;
import ca.uwaterloo.shediac.constant.IntConstant;
import ca.uwaterloo.shediac.constant.LongConstant;
import ca.uwaterloo.shediac.constant.ShortConstant;

public abstract class AbstractBufferHost implements BufferHost {

  protected final int size;
  protected final boolean pinned;
  protected Buffer buf;

  public AbstractBufferHost(final int size, final boolean pinned) {
    this.size = size;
    this.pinned = pinned;
  }

  @Override
  public int getSize() {
    return size;
  }

  @Override
  public void set(int idx, Constant val) {
    if (val instanceof IntConstant) {
      ((IntBuffer) buf).put(idx, (Integer) val.asJavaVal());
    } else if (val instanceof DoubleConstant) {
      ((DoubleBuffer) buf).put(idx, (Double) val.asJavaVal());
    } else if (val instanceof ShortConstant) {
      ((ShortBuffer) buf).put(idx, (Short) val.asJavaVal());
    } else if (val instanceof ByteConstant) {
      ((ByteBuffer) buf).put(idx, (Byte) val.asJavaVal());
    } else if (val instanceof FloatConstant) {
      ((FloatBuffer) buf).put(idx, (Float) val.asJavaVal());
    } else if (val instanceof LongConstant) {
      ((LongBuffer) buf).put(idx, (Long) val.asJavaVal());
    } else {
      throw new RuntimeException("Invalid or unsupported data type");
    }
  }

  @Override
  public Constant get(final int idx, final Type type) {
    switch (type) {
      case BYTE:
        return new ByteConstant(((ByteBuffer) buf).get(idx));
      case DOUBLE:
        return new DoubleConstant(((DoubleBuffer) buf).get(idx));
      case FLOAT:
        return new FloatConstant(((FloatBuffer) buf).get(idx));
      case INTEGER:
        return new IntConstant(((IntBuffer) buf).get(idx));
      default:
        throw new RuntimeException("Invalid or unsupported data type");
    }
  }

}
