package ca.uwaterloo.shediac.memory;

import ca.uwaterloo.shediac.constant.Constant;
import ca.uwaterloo.shediac.device.Queue;
import ca.uwaterloo.shediac.memory.Buffer.BufferAccess;

/**
 * A Memory object manages shared data between a host and device. Access constraints are specified
 * via a {@link BufferAccess} object. Copy operations are submitted to a {@link Queue} for
 * execution. A {@link Buffer} object references device memory, and a {@link BufferHost} references
 * host memory.
 */
public class Memory {
  final protected Queue queue;
  final protected Buffer dev;
  final protected BufferHost host;
  final protected long size;
  final Buffer.BufferAccess access;

  public Memory(final Queue q, final Buffer d, final BufferHost h) {
    queue = q;
    dev = d;
    host = h;
    this.size = dev.getSize();
    access = d.getAccess();
  }

  public Memory(final Queue q, final Buffer d, final BufferHost h, final Buffer.BufferAccess a) {
    queue = q;
    dev = d;
    host = h;
    this.size = dev.getSize();
    access = a;
    assert access.ordinal() == d.getAccess().ordinal();
  }

  public void set(final int idx, final Constant val) {
    if (host != null)
      host.set(idx, val);
    dev.set(queue, idx, val);
  }

  public Constant get(final int idx, final Type type) {
    if (host != null)
      return host.get(idx, type);
    else
      return dev.get(queue, idx, type);
  }

  public void copyHtoD() {
    dev.write(queue, size, host, 0L);
  }

  public void copyHtoDAsync() {
    dev.writeAsync(queue, size, host, 0L);
  }

  public void copyDtoH() {
    dev.read(queue, size, host, 0L);
  }

  public void copyDtoHAsync() {
    dev.readAsync(queue, size, host, 0L);
  }

  public void release() {
    host.release();
    dev.release();
  }

  public BufferHost getHostBuffer() {
    return host;
  }

  public Buffer getDeviceBuffer() {
    return dev;
  }

}
