package ca.uwaterloo.shediac.memory;

import java.nio.ByteBuffer;

import ca.uwaterloo.shediac.KernelMgr.DeviceType;

public final class Pointer {

  public DeviceType devType;
  private org.jocl.Pointer clPtr;
  private jcuda.Pointer cuPtr;
  private Type type;
  private Object data;
  private int size;

  private Pointer() {}

  public Pointer(final DeviceType type) {
    this.devType = type;
    if (type == DeviceType.CUDA) {
      cuPtr = new jcuda.Pointer();
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = new org.jocl.Pointer();
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public Pointer(final DeviceType type, final ByteBuffer bb) {
    this.devType = type;
    if (type == DeviceType.CUDA) {
      cuPtr = jcuda.Pointer.to(bb);
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = org.jocl.Pointer.to(bb);
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public Pointer(final DeviceType type, final int[] data) {
    this.devType = type;
    this.type = Type.INTEGER;
    this.size = data.length * Integer.BYTES;
    if (type == DeviceType.CUDA) {
      cuPtr = jcuda.Pointer.to(data);
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = org.jocl.Pointer.to(data);
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public Pointer(final DeviceType type, final float[] data) {
    this.devType = type;
    this.type = Type.FLOAT;
    this.size = data.length * Float.BYTES;
    if (type == DeviceType.CUDA) {
      cuPtr = jcuda.Pointer.to(data);
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = org.jocl.Pointer.to(data);
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public Pointer(final DeviceType type, final double[] data) {
    this.devType = type;
    this.type = Type.DOUBLE;
    this.size = data.length * Double.BYTES;
    if (type == DeviceType.CUDA) {
      cuPtr = jcuda.Pointer.to(data);
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = org.jocl.Pointer.to(data);
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public Pointer(final DeviceType type, final short[] data) {
    this.devType = type;
    this.type = Type.SHORT;
    this.size = data.length * Short.BYTES;
    if (type == DeviceType.CUDA) {
      cuPtr = jcuda.Pointer.to(data);
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = org.jocl.Pointer.to(data);
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public Pointer(final DeviceType type, final byte[] data) {
    this.devType = type;
    this.type = Type.BYTE;
    this.size = data.length * Byte.BYTES;
    if (type == DeviceType.CUDA) {
      cuPtr = jcuda.Pointer.to(data);
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = org.jocl.Pointer.to(data);
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public Pointer(final DeviceType type, final long[] data) {
    this.devType = type;
    this.type = Type.LONG;
    this.size = data.length * Long.BYTES;
    if (type == DeviceType.CUDA) {
      cuPtr = jcuda.Pointer.to(data);
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = org.jocl.Pointer.to(data);
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public Pointer(final DeviceType type, final char[] data) {
    this.devType = type;
    this.type = Type.CHAR;
    this.size = data.length * Character.BYTES;
    if (type == DeviceType.CUDA) {
      cuPtr = jcuda.Pointer.to(data);
      clPtr = null;
    } else if (type == DeviceType.OpenCL) {
      cuPtr = null;
      clPtr = org.jocl.Pointer.to(data);
    } else {
      throw new RuntimeException("Invalid device type specified when creating Pointer object.");
    }
  }

  public static <T> Pointer to(final T data) {
    Pointer p = new Pointer();
    p.devType = null; // not known at this time
    p.clPtr = null;
    p.cuPtr = null;
    p.data = data;
    if (data instanceof int[]) {
      final int[] tmp = (int[]) data;
      p.type = Type.INTEGER;
      p.size = tmp.length * Integer.BYTES;
    } else if (data instanceof float[]) {
      final float[] tmp = (float[]) data;
      p.type = Type.FLOAT;
      p.size = tmp.length * Float.BYTES;
    } else if (data instanceof double[]) {
      final double[] tmp = (double[]) data;
      p.type = Type.DOUBLE;
      p.size = tmp.length * Double.BYTES;
    } else if (data instanceof short[]) {
      final short[] tmp = (short[]) data;
      p.type = Type.SHORT;
      p.size = tmp.length * Short.BYTES;
    } else if (data instanceof long[]) {
      final long[] tmp = (long[]) data;
      p.type = Type.LONG;
      p.size = tmp.length * Long.BYTES;
    } else if (data instanceof byte[]) {
      final byte[] tmp = (byte[]) data;
      p.type = Type.BYTE;
      p.size = tmp.length * Byte.BYTES;
    } else if (data instanceof char[]) {
      final char[] tmp = (char[]) data;
      p.type = Type.CHAR;
      p.size = tmp.length * Character.BYTES;
    } else {
      throw new RuntimeException("Unsupported data type.");
    }
    return p;
  }

  public static Pointer to(final DeviceType type, final ByteBuffer bb) {
    return new Pointer(type, bb);
  }

  public static Pointer to(final DeviceType type, final int[] data) {
    return new Pointer(type, data);
  }

  public static Pointer to(final DeviceType type, final float[] data) {
    return new Pointer(type, data);
  }

  public static Pointer to(final DeviceType type, final double[] data) {
    return new Pointer(type, data);
  }

  public static Pointer to(final DeviceType type, final short[] data) {
    return new Pointer(type, data);
  }

  public static Pointer to(final DeviceType type, final byte[] data) {
    return new Pointer(type, data);
  }

  public static Pointer to(final DeviceType type, final long[] data) {
    return new Pointer(type, data);
  }

  public static Pointer to(final DeviceType type, final char[] data) {
    return new Pointer(type, data);
  }

  private static org.jocl.Pointer clTo(final Type dataType, final Object dataObj) {
    switch (dataType) {
      case INTEGER:
        return org.jocl.Pointer.to((int[]) dataObj);
      case FLOAT:
        return org.jocl.Pointer.to((float[]) dataObj);
      case DOUBLE:
        return org.jocl.Pointer.to((double[]) dataObj);
      case BYTE:
        return org.jocl.Pointer.to((byte[]) dataObj);
      case CHAR:
        return org.jocl.Pointer.to((char[]) dataObj);
      case LONG:
        return org.jocl.Pointer.to((long[]) dataObj);
      case SHORT:
        return org.jocl.Pointer.to((short[]) dataObj);
      default:
        throw new RuntimeException();
    }
  }

  private static jcuda.Pointer cuTo(final Type dataType, final Object dataObj) {
    switch (dataType) {
      case INTEGER:
        return jcuda.Pointer.to((int[]) dataObj);
      case FLOAT:
        return jcuda.Pointer.to((float[]) dataObj);
      case DOUBLE:
        return jcuda.Pointer.to((double[]) dataObj);
      case BYTE:
        return jcuda.Pointer.to((byte[]) dataObj);
      case CHAR:
        return jcuda.Pointer.to((char[]) dataObj);
      case LONG:
        return jcuda.Pointer.to((long[]) dataObj);
      case SHORT:
        return jcuda.Pointer.to((short[]) dataObj);
      default:
        throw new RuntimeException();
    }
  }

  @Override
  public String toString() {
    if (devType == DeviceType.CUDA)
      return cuPtr.toString();
    else
      return clPtr.toString();
  }

  public org.jocl.Pointer getPointerCl() {
    if (clPtr == null && data != null) {
      assert cuPtr == null;
      clPtr = Pointer.clTo(type, data);
    }
    return clPtr;
  }

  public jcuda.Pointer getPointerCu() {
    if (cuPtr == null && data != null) {
      assert clPtr == null;
      cuPtr = Pointer.cuTo(type, data);
    }
    return cuPtr;
  }

  public int getSize() {
    return size;
  }

  public Object getData() {
    return data;
  }

}
