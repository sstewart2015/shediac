package ca.uwaterloo.shediac;

import java.util.HashMap;
import java.util.Map;

import org.jocl.CL;

import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.device.Context;
import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Platform;
import ca.uwaterloo.shediac.device.Queue;
import ca.uwaterloo.shediac.device.cuda.CudaContext;
import ca.uwaterloo.shediac.device.cuda.CudaDevice;
import ca.uwaterloo.shediac.device.cuda.CudaPlatform;
import ca.uwaterloo.shediac.device.cuda.CudaQueue;
import ca.uwaterloo.shediac.device.opencl.clContext;
import ca.uwaterloo.shediac.device.opencl.clDevice;
import ca.uwaterloo.shediac.device.opencl.clPlatform;
import ca.uwaterloo.shediac.device.opencl.clQueue;
import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.BufferHost;
import ca.uwaterloo.shediac.memory.cuda.CudaBuffer;
import ca.uwaterloo.shediac.memory.cuda.CudaBufferHost;
import ca.uwaterloo.shediac.memory.opencl.clBuffer;
import ca.uwaterloo.shediac.memory.opencl.clBufferHost;
import jcuda.driver.JCudaDriver;

public class KernelGroup {

  public final DeviceType type;
  public final int platformId;
  public final int deviceId;
  private Map<Integer, KernelInfo> kernels = new HashMap<>();
  private int kernelIdx = 0;

  public final Platform platform;
  public final Device device;
  public final Context context;
  public final Queue queue;

  /**
   * Creates a kernel group for the specified device type, platform and device IDs. This constructor
   * is private and may only be called by the create methods, which will instantiate the Platform,
   * Device, Context, and Queue data structures for the appropriate device type.
   * 
   * @param type The device type (CUDA or OpenCL).
   * @param platformId The platform ID.
   * @param deviceId The device ID.
   * @param platform A reference to the platform used by the kernel group.
   * @param device A reference to the device used by the kernel group.
   * @param context A reference to the device context used by the kernel group.
   * @param queue A reference to the device command queue used by the kernel group.
   */
  private KernelGroup(final DeviceType type, final int platformId, final int deviceId,
      final Platform platform, final Device device, final Context context, final Queue queue) {
    this.type = type;
    this.platformId = platformId;
    this.deviceId = deviceId;
    this.platform = platform;
    this.device = device;
    this.context = context;
    this.queue = queue;
  }

  /**
   * Creates a kernel group for the specified type (CUDA or OpenCL).
   * 
   * @param type The device type (CUDA or OpenCL).
   * @param platformId The platform ID.
   * @param deviceId The device ID.
   * @param enableExceptions If true, JOCL exceptions are enabled.
   * @return A new KernelGroup object.
   */
  public static KernelGroup create(final DeviceType type, final int platformId, final int deviceId,
      boolean enableExceptions) {
    switch (type) {
      case CUDA:
        return create_CUDA(deviceId, enableExceptions);
      case OpenCL:
        return create_OpenCL(platformId, deviceId, enableExceptions);
      default:
        throw new RuntimeException("Unsupported device type \"" + type + "\"");
    }
  }

  /**
   * Creates a kernel group for CUDA kernels.
   * 
   * @param deviceId The device ID.
   * @param enableExceptions If true, JCUDA exceptions are enabled.
   * @return A new KernelGroup object.
   */
  public static KernelGroup create_CUDA(final int deviceId, boolean enableExceptions) {
    JCudaDriver.setExceptionsEnabled(enableExceptions);
    JCudaDriver.cuInit(0);
    final CudaPlatform p = new CudaPlatform(0);
    final CudaDevice d = new CudaDevice(p, deviceId, (c, size) -> {
      return (BufferHost) new CudaBufferHost(size, true);
    }, (c, ptr) -> {
      return (BufferHost) new CudaBufferHost(ptr);
    }, (c, config) -> {
      return (Buffer) new CudaBuffer(config);
    });
    final CudaContext context = new CudaContext(d);
    final CudaQueue q = new CudaQueue(context, d);
    return new KernelGroup(DeviceType.CUDA, 0, deviceId, p, d, context, q);
  }

  /**
   * Creates a kernel group for OpenCL kernels.
   * 
   * @param platformId The platform ID.
   * @param deviceId The device ID.
   * @param enableExceptions If true, JOCL exceptions are enabled.
   * @return A new KernelGroup object.
   */
  public static KernelGroup create_OpenCL(final int platformId, final int deviceId,
      final boolean enableExceptions) {
    CL.setExceptionsEnabled(enableExceptions);
    final clPlatform p = new clPlatform(platformId);
    final clDevice d = new clDevice(p, deviceId, (c, size) -> {
      return (BufferHost) new clBufferHost(size, true);
    }, (c, ptr) -> {
      return (BufferHost) new clBufferHost(ptr);
    }, (c, config) -> {
      return (Buffer) new clBuffer((clContext) c, config);
    });
    final clContext context = new clContext(d);
    final clQueue q = new clQueue(context, d);
    return new KernelGroup(DeviceType.OpenCL, platformId, deviceId, p, d, context, q);
  }

  public KernelInfo getKernel(final int kernelId) {
    if (kernelId < 0 || kernelId >= kernels.size())
      return null;
    return kernels.get(kernelId);
  }

  public int addKernel(final KernelInfo kInfo) {
    kernels.put(kernelIdx, kInfo);
    return kernelIdx++;
  }

  public void releaseKernel(final int kernelId) {
    final KernelInfo kInfo = getKernel(kernelId);
    kInfo.event.release();
    kInfo.kernel.release();
    kInfo.program.release();
    kernels.remove(kernelId);
  }

}
