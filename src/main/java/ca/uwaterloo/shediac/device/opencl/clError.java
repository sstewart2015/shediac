package ca.uwaterloo.shediac.device.opencl;

import org.jocl.CL;

public final class clError {

	private clError() {}
	
	public static void checkError(final int err) {
		if (err != CL.CL_SUCCESS)
			throw new org.jocl.CLException("Internal OpenCL error: " + err);
	}

	public static void checkError(int[] err) {
		checkError(err[0]);
	}
	
}
