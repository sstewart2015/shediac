package ca.uwaterloo.shediac.device.opencl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.jocl.CL;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_device_id;
import org.jocl.cl_program;

import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Program;

public class clProgram implements Program {

	final cl_program program;
	long length;
	String source;

	public clProgram(final clContext context, final String filename) {
		String source = "";
		try {
			source = readFile(filename, Charset.defaultCharset());
		} catch (IOException e) {
			throw new org.jocl.CLException("Unable to read file: " + filename);
		}
		int[] err = new int[1];
		program = CL.clCreateProgramWithSource(context.context, 1,
				new String[] { source }, new long[] { source.length() }, err);
		clError.checkError(err);
	}

	@Override
	public BuildStatus build(final Device d, final List<String> options) {
		final StringBuilder allOptions = new StringBuilder();
		for (final String str : options)
			allOptions.append(str + " ");
		final cl_device_id dev = ((clDevice) d).device;
		int status = CL.clBuildProgram(program, 1, new cl_device_id[] { dev },
				allOptions.toString(), null, null);
		if (status == CL.CL_SUCCESS)
			return BuildStatus.SUCCESS;
		else if (status == CL.CL_INVALID_BINARY)
			return BuildStatus.INVALID;
		else
			return BuildStatus.ERROR;
	}

	@Override
	public String getBuildInfo(final Device d) {
		long[] bytes = new long[1];
		final cl_device_id device = ((clDevice) d).device;
		CL.clGetProgramBuildInfo(program, device, CL.CL_PROGRAM_BUILD_LOG, 0,
				null, bytes);
		byte[] result = new byte[(int) bytes[0]];
		CL.clGetProgramBuildInfo(program, device, CL.CL_PROGRAM_BUILD_LOG,
				bytes[0], Pointer.to(result), null);
		return new String(result);
	}

	@Override
	public String getIR() {
		long[] bytes = new long[1];
		CL.clGetProgramInfo(program, CL.CL_PROGRAM_BINARY_SIZES,
				Sizeof.size_t, Pointer.to(bytes), null);
		byte result[][] = new byte[1][];
		result[0] = new byte[(int)bytes[0]];
		Pointer[] ptrResults = new Pointer[] {Pointer.to(result[0])};
		CL.clGetProgramInfo(program, CL.CL_PROGRAM_BINARIES, Sizeof.POINTER,
				Pointer.to(ptrResults), null);
		return new String(result[0]);
	}

	/**
	 * This methods returns a string containing the content of a text file whose
	 * location is specified by path.
	 *
	 * @param path
	 *            The location of the file.
	 * @param encoding
	 *            The character encoding of the file.
	 * @return A String containing the contents of the file.
	 * @throws IOException
	 */
	private static String readFile(final String path, final Charset encoding)
			throws IOException {
		final byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	@Override
	public void release() {
		clError.checkError(CL.clReleaseProgram(program));
	}

}
