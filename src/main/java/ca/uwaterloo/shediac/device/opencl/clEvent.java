package ca.uwaterloo.shediac.device.opencl;

import org.jocl.CL;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_event;

import ca.uwaterloo.shediac.device.Event;

public class clEvent implements Event {

	final cl_event event = new cl_event();

	public clEvent() {
	}

	@Override
	public void waitForCompletion() {
		CL.clWaitForEvents(1, new cl_event[] { event });
	}

	@Override
	public float getElapsedTime() {
		long[] startTime = new long[1];
		long[] endTime = new long[1];
		waitForCompletion();
		CL.clGetEventProfilingInfo(event, CL.CL_PROFILING_COMMAND_START,
				Sizeof.cl_ulong, Pointer.to(startTime), null);
		CL.clGetEventProfilingInfo(event, CL.CL_PROFILING_COMMAND_END,
				Sizeof.cl_ulong, Pointer.to(endTime), null);
		return (endTime[0] - startTime[0]) * 1.0e-6f;
	}

	@Override
	public void release() {
		clError.checkError(CL.clReleaseEvent(event));
	}

	
}
