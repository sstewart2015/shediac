package ca.uwaterloo.shediac.device.opencl;

import java.util.function.BiFunction;

import org.jocl.CL;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_device_id;

import ca.uwaterloo.shediac.device.AbstractDevice;
import ca.uwaterloo.shediac.device.Context;
import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.BufferConfig;
import ca.uwaterloo.shediac.memory.BufferHost;

public class clDevice extends AbstractDevice {

  final cl_device_id device;

  public clDevice(final clPlatform platform, final int deviceId,
      final BiFunction<Context, Integer, BufferHost> hostAllocator,
      final BiFunction<Context, ca.uwaterloo.shediac.memory.Pointer, BufferHost> hostAllocatorData,
      final BiFunction<Context, BufferConfig, Buffer> deviceAllocator) {
    super(hostAllocator, hostAllocatorData, deviceAllocator);
    int[] numDevices = new int[] {platform.numDevices()};
    if (numDevices[0] == 0)
      throw new org.jocl.CLException("No OpenCL devices found.");
    if (deviceId >= numDevices[0])
      throw new org.jocl.CLException("Invalid device ID.");
    cl_device_id[] devices = new cl_device_id[numDevices[0]];
    CL.clGetDeviceIDs(platform.platform, CL.CL_DEVICE_TYPE_ALL, numDevices[0], devices, null);
    device = devices[deviceId];
  }

  private String getInfoString(final int info) {
    long[] bytes = new long[1];
    CL.clGetDeviceInfo(device, info, 0, null, bytes);
    byte[] result = new byte[(int) bytes[0]];
    CL.clGetDeviceInfo(device, info, bytes[0], Pointer.to(result), null);
    return new String(result);
  }

  private int getInfoInteger(final int info) {
    int[] result = new int[1];
    CL.clGetDeviceInfo(device, info, Sizeof.cl_int, Pointer.to(result), null);
    return result[0];
  }

  private long getInfoLong(final int info) {
    long[] result = new long[1];
    CL.clGetDeviceInfo(device, info, Sizeof.cl_long, Pointer.to(result), null);
    return result[0];
  }

  @Override
  public String version() {
    return getInfoString(CL.CL_DEVICE_VERSION);
  }

  @Override
  public String vendor() {
    return getInfoString(CL.CL_DEVICE_VENDOR);
  }

  @Override
  public String name() {
    return getInfoString(CL.CL_DEVICE_NAME);
  }

  @Override
  public String type() {
    // TODO
    return "";
  }

  @Override
  public long maxWorkGroupSize() {
    return getInfoLong(CL.CL_DEVICE_MAX_WORK_GROUP_SIZE);
  }

  @Override
  public long maxWorkItemDimensions() {
    return getInfoLong(CL.CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS);
  }

  @Override
  public long[] maxWorkItemSizes() {
    // return getInfoLongArr(CL.CL_DEVICE_MAX_WORK_ITEM_SIZES);
    // TODO
    return null;
  }

  @Override
  public int localMemSize() {
    return (int) getInfoLong(CL.CL_DEVICE_LOCAL_MEM_SIZE);
  }

  @Override
  public String capabilities() {
    return getInfoString(CL.CL_DEVICE_EXTENSIONS);
  }

  @Override
  public int coreClock() {
    return getInfoInteger(CL.CL_DEVICE_MAX_CLOCK_FREQUENCY);
  }

  @Override
  public int computeUnits() {
    return getInfoInteger(CL.CL_DEVICE_MAX_COMPUTE_UNITS);
  }

  @Override
  public long memorySize() {
    return getInfoLong(CL.CL_DEVICE_GLOBAL_MEM_SIZE);
  }

  @Override
  public long maxAllocSize() {
    return getInfoLong(CL.CL_DEVICE_MAX_MEM_ALLOC_SIZE);
  }

  @Override
  public int memoryClock() {
    // Not available in OpenCL
    return 0;
  }

  @Override
  public int memoryBusWidth() {
    // Not available in OpenCL
    return 0;
  }

  @Override
  public boolean isLocalMemoryValid(long localMemUsage) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean isThreadConfigValid(long[] local) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void release() {
    clError.checkError(CL.clReleaseDevice(device));
  }

}
