package ca.uwaterloo.shediac.device.opencl;

import org.jocl.CL;
import org.jocl.cl_command_queue;

import ca.uwaterloo.shediac.device.Context;
import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Event;
import ca.uwaterloo.shediac.device.Queue;

public class clQueue implements Queue {

	public final clContext context;
	public final clDevice device;
	public final cl_command_queue queue;

	public clQueue(final clContext c, final clDevice d) {
		context = c;
		device = d;
		final int[] err = new int[1];
		queue = CL.clCreateCommandQueue(c.context, d.device,
				CL.CL_QUEUE_PROFILING_ENABLE, err);
		clError.checkError(err);
	}

	@Override
	public void finish(Event e) {
		finish();
	}

	@Override
	public void finish() {
		CL.clFinish(queue);
	}

	@Override
	public Context getContext() {
		return context;
	}

	@Override
	public Device getDevice() {
		return device;
	}

	@Override
	public void release() {
		clError.checkError(CL.clReleaseCommandQueue(queue));
	}

}
