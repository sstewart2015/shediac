package ca.uwaterloo.shediac.device.opencl;

import org.jocl.CL;
import org.jocl.cl_context;
import org.jocl.cl_device_id;

import ca.uwaterloo.shediac.device.Context;

public class clContext implements Context {

	public final cl_context context;

	public clContext(clDevice d) {
		int[] err = new int[1];
		final cl_device_id[] dev = new cl_device_id[] { d.device };
		context = CL.clCreateContext(null, 1, dev, null, null, err);
		clError.checkError(err);
		assert context != null;
	}

	public clContext(final cl_context c) {
		context = c;
	}

	@Override
	public void release() {
		clError.checkError(CL.clReleaseContext(context));
	}

}
