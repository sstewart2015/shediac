package ca.uwaterloo.shediac.device.opencl;

import java.util.ArrayList;
import java.util.List;

import org.jocl.CL;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_command_queue;
import org.jocl.cl_event;
import org.jocl.cl_kernel;

import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Event;
import ca.uwaterloo.shediac.device.Kernel;
import ca.uwaterloo.shediac.device.Queue;
import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.opencl.clBuffer;

public class clKernel implements Kernel {

  final cl_kernel kernel;
  List<Pointer> arguments = new ArrayList<>();
  List<Integer> argumentSizes = new ArrayList<>();

  public clKernel(final clProgram prog, final String name) {
    int[] err = new int[1];
    kernel = CL.clCreateKernel(prog.program, name, err);
    clError.checkError(err);
  }

  @Override
  public void setArgument(int idx, Buffer val) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, Pointer.to(((clBuffer) val).buf));
    argumentSizes.set(idx, Sizeof.cl_mem);
  }

  @Override
  public void setArgument(int idx, int val) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, Pointer.to(new int[] {val}));
    argumentSizes.set(idx, Sizeof.cl_int);
  }

  @Override
  public void setArgument(int idx, float val) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, Pointer.to(new float[] {val}));
    argumentSizes.set(idx, Sizeof.cl_float);
  }

  @Override
  public void setArgument(int idx, double val) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, Pointer.to(new double[] {val}));
    argumentSizes.set(idx, Sizeof.cl_double);
  }

  @Override
  public void setArgument(int idx, short val) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, Pointer.to(new short[] {val}));
    argumentSizes.set(idx, Sizeof.cl_short);
  }

  @Override
  public void setArgument(int idx, long val) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, Pointer.to(new long[] {val}));
    argumentSizes.set(idx, Sizeof.cl_long);
  }

  @Override
  public void setArgument(int idx, char val) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, Pointer.to(new char[] {val}));
    argumentSizes.set(idx, Sizeof.cl_char);
  }

  @Override
  public void setArgument(int idx, byte val) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, Pointer.to(new byte[] {val}));
    argumentSizes.set(idx, Sizeof.cl_char8);
  }

  @Override
  public void setArgumentLocal(final int idx, final int localBytes) {
    for (int i = arguments.size(); i <= idx; i++) {
      arguments.add(null);
      argumentSizes.add(0);
    }
    arguments.set(idx, null);
    argumentSizes.set(idx, localBytes);
  }

  @Override
  public long localMemUsage(Device d) {
    long[] bytes = new long[1];
    CL.clGetKernelWorkGroupInfo(kernel, ((clDevice) d).device, CL.CL_KERNEL_LOCAL_MEM_SIZE, 0, null,
        bytes);
    long[] result = new long[1];
    CL.clGetKernelWorkGroupInfo(kernel, ((clDevice) d).device, CL.CL_KERNEL_LOCAL_MEM_SIZE, 0,
        Pointer.to(result), null);
    return result[0];
  }

  private void prepareArguments() {
    for (int i = 0; i < arguments.size(); i++) {
      final long size = argumentSizes.get(i);
      final Pointer ptr = arguments.get(i);
      clError.checkError(CL.clSetKernelArg(kernel, i, size, ptr));
    }
  }

  @Override
  public void launch(Queue q, long[] global, long[] local, Event e) {
    prepareArguments();
    cl_command_queue queue = ((clQueue) q).queue;
    cl_event event = ((clEvent) e).event;
    clError.checkError(CL.clEnqueueNDRangeKernel(queue, kernel, global.length, null, global, local,
        0, null, event));
  }

  @Override
  public void launch(Queue q, long[] global, long[] local, Event e, List<Event> waitForEvents) {
    if (waitForEvents.size() == 0)
      launch(q, global, local, e);
    else {
      cl_event[] events = new cl_event[waitForEvents.size()];
      for (int i = 0; i < events.length; i++)
        events[i] = ((clEvent) waitForEvents.get(i)).event;

      // Launches the kernel while waiting for other events
      prepareArguments();
      cl_command_queue queue = ((clQueue) q).queue;
      cl_event event = ((clEvent) e).event;
      clError.checkError(CL.clEnqueueNDRangeKernel(queue, kernel, global.length, null, global,
          local, events.length, events, event));
    }
  }

  @Override
  public void launch(Queue q, long[] global, Event e) {
    prepareArguments();
    cl_command_queue queue = ((clQueue) q).queue;
    cl_event event = ((clEvent) e).event;
    clError.checkError(CL.clEnqueueNDRangeKernel(queue, kernel, global.length, null, global, null,
        0, null, event));
  }

  @Override
  public void release() {
    clError.checkError(CL.clReleaseKernel(kernel));
  }

}
