package ca.uwaterloo.shediac.device.opencl;

import org.jocl.CL;
import org.jocl.cl_platform_id;

import ca.uwaterloo.shediac.device.Platform;

public class clPlatform implements Platform {

	final cl_platform_id platform;

	/**
	 * Creates an OpenCL platform reference for the specified platform ID.
	 */
	public clPlatform(final int platformId) {
		final int[] numPlatforms = new int[1];
		CL.clGetPlatformIDs(0, null, numPlatforms);
		if (numPlatforms[0] == 0)
			throw new org.jocl.CLException("No platforms found.");
		if (platformId >= numPlatforms[0])
			throw new org.jocl.CLException("Invalid platform ID.");
		cl_platform_id[] platforms = new cl_platform_id[numPlatforms[0]];
		CL.clGetPlatformIDs(numPlatforms[0], platforms, null);
		platform = platforms[platformId];
	}

	@Override
	public int numDevices() {
		int[] numDevices = new int[1];
		CL.clGetDeviceIDs(platform, CL.CL_DEVICE_TYPE_ALL, 0, null, numDevices);
		return numDevices[0];
	}

}
