package ca.uwaterloo.shediac.device;

public interface Queue {

	void finish(Event e);
	
	void finish();
	
	Context getContext();
	
	Device getDevice();
	
	void release();
}
