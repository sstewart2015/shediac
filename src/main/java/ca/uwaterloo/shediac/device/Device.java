package ca.uwaterloo.shediac.device;

import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.BufferConfig;
import ca.uwaterloo.shediac.memory.BufferHost;
import ca.uwaterloo.shediac.memory.Pointer;

public interface Device {

  BufferHost allocateHost(Context c, Integer size);

  BufferHost allocateHostData(Context c, Pointer p);

  Buffer allocateDevice(Context c, BufferConfig config);

  void release();

  String version();

  String vendor();

  String name();

  String type();

  long maxWorkGroupSize();

  long maxWorkItemDimensions();

  long[] maxWorkItemSizes();

  int localMemSize();

  String capabilities();

  int coreClock();

  int computeUnits();

  long memorySize();

  long maxAllocSize();

  int memoryClock();

  int memoryBusWidth();

  boolean isLocalMemoryValid(long localMemUsage);

  boolean isThreadConfigValid(long[] local);

}
