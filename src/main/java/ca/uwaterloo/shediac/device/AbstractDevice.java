package ca.uwaterloo.shediac.device;

import java.util.function.BiFunction;

import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.BufferConfig;
import ca.uwaterloo.shediac.memory.BufferHost;
import ca.uwaterloo.shediac.memory.Pointer;

public abstract class AbstractDevice implements Device {

  protected final BiFunction<Context, Integer, BufferHost> hostAllocator;
  protected final BiFunction<Context, Pointer, BufferHost> hostAllocatorData;
  protected final BiFunction<Context, BufferConfig, Buffer> deviceAllocator;

  public AbstractDevice(final BiFunction<Context, Integer, BufferHost> hostAllocator,
      final BiFunction<Context, Pointer, BufferHost> hostAllocatorData,
      final BiFunction<Context, BufferConfig, Buffer> deviceAllocator) {
    this.hostAllocator = hostAllocator;
    this.hostAllocatorData = hostAllocatorData;
    this.deviceAllocator = deviceAllocator;
  }

  @Override
  public BufferHost allocateHost(final Context c, final Integer size) {
    return this.hostAllocator.apply(c, size);
  }

  @Override
  public BufferHost allocateHostData(final Context c, final Pointer p) {
    return this.hostAllocatorData.apply(c, p);
  }

  @Override
  public Buffer allocateDevice(final Context c, final BufferConfig config) {
    return this.deviceAllocator.apply(c, config);
  }

}
