package ca.uwaterloo.shediac.device;

import java.util.List;

public interface Program {

	public enum BuildStatus {
		SUCCESS, ERROR, INVALID
	}

	BuildStatus build(Device d, List<String> options);
	
	String getBuildInfo(Device d);
	
	String getIR();
	
	void release();
	
}
