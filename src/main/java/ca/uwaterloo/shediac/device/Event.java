package ca.uwaterloo.shediac.device;

public interface Event {

	/** Waits for the completion of this event (not implemented for CUDA). */
	void waitForCompletion();
	
	/** Retrieves the elapsed time of the last recorded event. */
	float getElapsedTime();
	
	void release();
}
