package ca.uwaterloo.shediac.device;

import java.util.List;

import ca.uwaterloo.shediac.memory.Buffer;

public interface Kernel {

  void setArgument(int idx, Buffer val);

  void setArgument(int idx, int val);

  void setArgument(int idx, float val);

  void setArgument(int idx, double val);
  
  void setArgument(int idx, long val);
  
  void setArgument(int idx, short val);
  
  void setArgument(int idx, char val);
  
  void setArgument(int idx, byte val);
  
  void setArgumentLocal(int i, int localBytes);

  long localMemUsage(Device d);

  void launch(Queue q, long[] global, long[] local, Event e);

  void launch(Queue q, long[] global, long[] local, Event e, List<Event> waitForEvents);

  void launch(Queue q, long[] global, Event e);

  void release();

}
