package ca.uwaterloo.shediac.device.cuda;

import jcuda.driver.CUevent;
import jcuda.driver.CUevent_flags;
import jcuda.driver.JCudaDriver;
import ca.uwaterloo.shediac.device.Event;

public class CudaEvent implements Event {

	final CUevent start = new CUevent();
	final CUevent stop = new CUevent();

	public CudaEvent() {
		JCudaDriver.cuEventCreate(start, CUevent_flags.CU_EVENT_DEFAULT);
		JCudaDriver.cuEventCreate(stop, CUevent_flags.CU_EVENT_DEFAULT);
	}

	@Override
	public void waitForCompletion() {
		// Not implemented for CUDA
	}

	@Override
	public float getElapsedTime() {
		float[] result = new float[1];
		JCudaDriver.cuEventElapsedTime(result, start, stop);
		return result[0];
	}

	@Override
	public void release() {
		JCudaDriver.cuEventDestroy(start);
		JCudaDriver.cuEventDestroy(stop);
	}

}
