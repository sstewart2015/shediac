package ca.uwaterloo.shediac.device.cuda;

import jcuda.driver.CUstream;
import jcuda.driver.JCudaDriver;
import ca.uwaterloo.shediac.device.Context;
import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Event;
import ca.uwaterloo.shediac.device.Queue;

public class CudaQueue implements Queue {

	public final CudaContext context;
	public final CudaDevice device;
	public CUstream queue;

	public CudaQueue(final CudaContext context, final CudaDevice device) {
		queue = new CUstream();
		this.context = context;
		this.device = device;
	}

	@Override
	public void finish(Event e) {
		JCudaDriver.cuEventSynchronize(((CudaEvent) e).stop);
		finish();
	}

	@Override
	public void finish() {
		JCudaDriver.cuStreamSynchronize(queue);
	}

	@Override
	public Context getContext() {
		return context;
	}

	@Override
	public Device getDevice() {
		return device;
	}

	@Override
	public void release() {
		// TODO Auto-generated method stub
	}

}
