package ca.uwaterloo.shediac.device.cuda;

import java.util.function.BiFunction;

import ca.uwaterloo.shediac.device.AbstractDevice;
import ca.uwaterloo.shediac.device.Context;
import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.BufferConfig;
import ca.uwaterloo.shediac.memory.BufferHost;
import ca.uwaterloo.shediac.memory.Pointer;
import jcuda.driver.CUdevice;
import jcuda.driver.CUdevice_attribute;
import jcuda.driver.JCudaDriver;

/** Wrapper for a CUdevice object. */
public class CudaDevice extends AbstractDevice {

  final CUdevice device;

  public CudaDevice(final CudaPlatform platform, final int deviceId,
      final BiFunction<Context, Integer, BufferHost> hostAllocator,
      final BiFunction<Context, Pointer, BufferHost> hostAllocatorData,
      final BiFunction<Context, BufferConfig, Buffer> deviceAllocator) {
    super(hostAllocator, hostAllocatorData, deviceAllocator);
    device = new CUdevice();
    final int numDevices = platform.numDevices();
    JCudaDriver.cuDeviceGet(device, deviceId % numDevices);
  }

  public CudaDevice(final CUdevice d, final BiFunction<Context, Integer, BufferHost> hostAllocator,
      final BiFunction<Context, Pointer, BufferHost> hostAllocatorData,
      final BiFunction<Context, BufferConfig, Buffer> deviceAllocator) {
    super(hostAllocator, hostAllocatorData, deviceAllocator);
    device = d;
  }

  @Override
  public String version() {
    int[] result = new int[1];
    JCudaDriver.cuDriverGetVersion(result);
    return Integer.toString(result[0]);
  }

  @Override
  public String vendor() {
    return "NVIDIA Corporation";
  }

  @Override
  public String name() {
    byte[] result = new byte[256];
    JCudaDriver.cuDeviceGetName(result, 256, device);
    return new String(result);
  }

  @Override
  public String type() {
    return "GPU";
  }

  @Override
  public long maxWorkGroupSize() {
    return getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK);
  }

  @Override
  public long maxWorkItemDimensions() {
    return 3L;
  }

  @Override
  public long[] maxWorkItemSizes() {
    return new long[] {getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X),
        getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y),
        getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z)};
  }

  @Override
  public int localMemSize() {
    return getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK);
  }

  @Override
  public String capabilities() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public int coreClock() {
    return getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_CLOCK_RATE);
  }

  @Override
  public int computeUnits() {
    return getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT);
  }

  @Override
  public long memorySize() {
    long[] result = new long[1];
    JCudaDriver.cuDeviceTotalMem(result, device);
    return result[0];
  }

  @Override
  public long maxAllocSize() {
    return memorySize();
  }

  @Override
  public int memoryClock() {
    return getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE);
  }

  @Override
  public int memoryBusWidth() {
    return getInfo(CUdevice_attribute.CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH);
  }

  @Override
  public boolean isLocalMemoryValid(final long localMemUsage) {
    return (localMemUsage <= localMemSize());
  }

  @Override
  public boolean isThreadConfigValid(final long[] local) {
    if (local.length > maxWorkItemDimensions())
      return false;
    long localSize = 1;
    for (int i = 0; i < local.length; i++) {
      if (local[i] > maxWorkItemSizes()[i])
        return false;
      localSize *= local[i];
    }
    if (localSize > maxWorkGroupSize())
      return false;
    return true;
  }

  /** Helper method for retrieving device attributes. */
  private int getInfo(final int info) {
    int[] result = new int[1];
    JCudaDriver.cuDeviceGetAttribute(result, info, device);
    return result[0];
  }

  @Override
  public void release() {
    // TODO Auto-generated method stub
  }

}
