package ca.uwaterloo.shediac.device.cuda;

import java.util.ArrayList;
import java.util.List;

import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Event;
import ca.uwaterloo.shediac.device.Kernel;
import ca.uwaterloo.shediac.device.Queue;
import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.cuda.CudaBuffer;
import jcuda.Pointer;
import jcuda.driver.CUevent;
import jcuda.driver.CUfunction;
import jcuda.driver.CUfunction_attribute;
import jcuda.driver.CUmodule;
import jcuda.driver.CUstream;
import jcuda.driver.JCudaDriver;

public class CudaKernel implements Kernel {

  CUmodule module;
  CUfunction function;
  List<Pointer> arguments = new ArrayList<>();

  public CudaKernel(final CUmodule module, final CUfunction function) {
    this.module = module;
    this.function = function;
  }

  public CudaKernel(final CudaProgram program, final String name) {
    module = new CUmodule();
    function = new CUfunction();
    JCudaDriver.cuModuleLoad(module, program.ptxFilename);
    JCudaDriver.cuModuleGetFunction(function, module, name);
  }

  @Override
  public void setArgument(int idx, int val) {
    for (int i = arguments.size(); i <= idx; i++)
      arguments.add(null);
    arguments.set(idx, Pointer.to(new int[] {val}));
  }

  @Override
  public void setArgument(int idx, float val) {
    for (int i = arguments.size(); i <= idx; i++)
      arguments.add(null);
    arguments.set(idx, Pointer.to(new float[] {val}));
  }

  @Override
  public void setArgument(int idx, double val) {
    for (int i = arguments.size(); i <= idx; i++)
      arguments.add(null);
    arguments.set(idx, Pointer.to(new double[] {val}));
  }

  @Override
  public void setArgument(int idx, long val) {
    for (int i = arguments.size(); i <= idx; i++)
      arguments.add(null);
    arguments.set(idx, Pointer.to(new long[] {val}));
  }

  @Override
  public void setArgument(int idx, short val) {
    for (int i = arguments.size(); i <= idx; i++)
      arguments.add(null);
    arguments.set(idx, Pointer.to(new short[] {val}));
  }

  @Override
  public void setArgument(int idx, char val) {
    for (int i = arguments.size(); i <= idx; i++)
      arguments.add(null);
    arguments.set(idx, Pointer.to(new char[] {val}));
  }

  @Override
  public void setArgument(int idx, byte val) {
    for (int i = arguments.size(); i <= idx; i++)
      arguments.add(null);
    arguments.set(idx, Pointer.to(new byte[] {val}));
  }

  @Override
  public void setArgument(int idx, Buffer val) {
    for (int i = arguments.size(); i <= idx; i++)
      arguments.add(null);
    arguments.set(idx, Pointer.to(((CudaBuffer) val).ptr));
  }

  @Override
  public long localMemUsage(Device d) {
    int[] result = new int[1];
    JCudaDriver.cuFuncGetAttribute(result, CUfunction_attribute.CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES,
        function);
    return (long) result[1];
  }

  @Override
  public void launch(Queue q, long[] global, long[] local, Event e) {
    final CUstream stream = ((CudaQueue) q).queue;
    final CUevent start = ((CudaEvent) e).start;
    final CUevent stop = ((CudaEvent) e).stop;

    // Configure grid and block for launch
    final long[] grid = new long[] {1, 1, 1};
    final long[] block = new long[] {1, 1, 1};
    if (global.length != local.length) {
      throw new jcuda.CudaException("Invalid thread/workgroup dimensions.");
    }
    for (int i = 0; i < local.length; i++) {
      grid[i] = (global[i] + local[i] - 1) / local[i];
      block[i] = local[i];
    }

    // Prepare the kernel arguments
    Pointer[] argArr = new Pointer[arguments.size()];
    for (int i = 0; i < arguments.size(); i++)
      argArr[i] = arguments.get(i);

    // Launch the kernel
    JCudaDriver.cuEventRecord(start, stream);
    JCudaDriver.cuLaunchKernel(function, (int) grid[0], (int) grid[1], (int) grid[2],
        (int) block[0], (int) block[1], (int) block[2], 0, stream, Pointer.to(argArr), null);
    JCudaDriver.cuEventRecord(stop, stream);
    JCudaDriver.cuEventSynchronize(stop);
  }

  @Override
  public void launch(Queue q, long[] global, long[] local, Event e, List<Event> waitForEvents) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void launch(Queue q, long[] global, Event e) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void release() {
    // TODO Auto-generated method stub
  }

  @Override
  public void setArgumentLocal(final int i, final int localBytes) {
    throw new UnsupportedOperationException(); // TODO Implement this method!
  }

}
