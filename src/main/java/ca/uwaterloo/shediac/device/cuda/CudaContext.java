package ca.uwaterloo.shediac.device.cuda;

import jcuda.driver.CUcontext;
import jcuda.driver.CUdevice;
import jcuda.driver.JCudaDriver;
import ca.uwaterloo.shediac.device.Context;

public class CudaContext implements Context {

	private CUcontext context;
	
	public CudaContext(final CUcontext context) {
		this.context = context;
	}
	
	public CudaContext(final CudaDevice device) {
		context = new CUcontext();
		JCudaDriver.cuCtxCreate(context, 0, device.device);
	}
	
	public CudaContext(final CUdevice device) {
		context = new CUcontext();
		JCudaDriver.cuCtxCreate(context, 0, device);
	}

	@Override
	public void release() {
		JCudaDriver.cuCtxDestroy(context);
	}
	
}
