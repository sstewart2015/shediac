package ca.uwaterloo.shediac.device.cuda;

import jcuda.driver.JCudaDriver;
import ca.uwaterloo.shediac.device.Platform;

public class CudaPlatform implements Platform {

	final int platformId;
	
	/** Note that the platform ID is not actually used for CUDA. */
	public CudaPlatform(final int platformId) {
		this.platformId = platformId;
	}
	
	@Override
	public int numDevices() {
		int[] count = new int[1];
		JCudaDriver.cuDeviceGetCount(count);
		return count[0];
	}

}
