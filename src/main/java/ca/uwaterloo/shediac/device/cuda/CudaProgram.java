package ca.uwaterloo.shediac.device.cuda;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Program;

public class CudaProgram implements Program {

	final String filename;
	final String ptxFilename;
	String buildInfo = "";

	public CudaProgram(final CudaContext context, final String filename) {
		this.filename = filename;
		int endIndex = filename.lastIndexOf('.');
		if (endIndex == -1)
			endIndex = filename.length() - 1;
		this.ptxFilename = filename.substring(0, endIndex + 1) + "ptx";
	}

	@Override
	public BuildStatus build(Device d, List<String> options) {
		// Check that the CUDA program file exists
		File cuFile = new File(filename);
		if (!cuFile.exists()) {
			buildInfo = "Input file not found: " + filename;
			return BuildStatus.ERROR;
		}

		// Prepare the PTX file
		try {
			buildInfo = preparePtxFile(cuFile);
		} catch (IOException e) {
			e.printStackTrace();
			return BuildStatus.ERROR;
		}
		if (!buildInfo.isEmpty()) {
			return BuildStatus.ERROR;
		}
		return BuildStatus.SUCCESS;
	}

	@Override
	public String getBuildInfo(Device d) {
		return buildInfo;
	}

	@Override
	public String getIR() {
		try {
			String ptx = readFile(ptxFilename, Charset.defaultCharset());
			return ptx;
		} catch (IOException e) {
			return "";
		}
	}

	/**
	 * Given the source file, produces a new file containing the ptx
	 * (intermediate representation) of the program. This method achieves this
	 * by calling the nvcc compiler on the source file with the -ptx option.
	 * 
	 * @param cuFile
	 *            The source file.
	 * @return The output (including error messages) of the compiler.
	 * @throws IOException
	 */
	private String preparePtxFile(final File cuFile) throws IOException {
		// Prepare command for running nvcc
		String modelString = "-m" + System.getProperty("sun.arch.data.model");
		String command = "nvcc " + modelString + " -ptx " + cuFile.getPath()
				+ " -o " + ptxFilename;

		// Launch a process with the command
		Process process = Runtime.getRuntime().exec(command);
		String errorMessage = new String(toByteArray(process.getErrorStream()));
		String outputMessage = new String(toByteArray(process.getInputStream()));

		// Check the result of running the command
		int exitValue = 0;
		try {
			exitValue = process.waitFor();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new IOException("Interrupted while waiting for nvcc output",
					e);
		}
		if (exitValue != 0) {
			return errorMessage + "\n\n" + outputMessage;
		}
		return "";

	}

	/**
	 * Fully reads the given InputStream and returns it as a byte array
	 *
	 * @param inputStream
	 *            The input stream to read
	 * @return The byte array containing the data from the input stream
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private static byte[] toByteArray(InputStream inputStream)
			throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte buffer[] = new byte[8192];
		while (true) {
			int read = inputStream.read(buffer);
			if (read == -1) {
				break;
			}
			baos.write(buffer, 0, read);
		}
		return baos.toByteArray();
	}

	/**
	 * This methods returns a string containing the content of a text file whose
	 * location is specified by path.
	 *
	 * @param path
	 *            The location of the file.
	 * @param encoding
	 *            The character encoding of the file.
	 * @return A String containing the contents of the file.
	 * @throws IOException
	 */
	public static String readFile(final String path, final Charset encoding)
			throws IOException {
		final byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	@Override
	public void release() {
		// TODO
	}

}
