package ca.uwaterloo.shediac.device;

public interface Platform {

	/** Returns the number of devices on this platform. */
	int numDevices();
	
}
