package ca.uwaterloo.shediac.constant;

/**
 * An interface that denotes values.
 */
public interface Constant extends Comparable<Constant> {
  /** Unwraps the constant and returns it. */
  public Object asJavaVal();
}
