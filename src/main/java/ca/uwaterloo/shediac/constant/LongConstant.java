package ca.uwaterloo.shediac.constant;

public class LongConstant implements Constant {

  private Long val;

  public LongConstant(final long n) {
    val = n;
  }

  @Override
  public int compareTo(final Constant obj) {
    final LongConstant k = (LongConstant) obj;
    return val.compareTo(k.val);
  }

  @Override
  public Object asJavaVal() {
    return val;
  }

  @Override
  public boolean equals(final Object obj) {
    LongConstant k = (LongConstant) obj;
    return k != null && val.equals(k.val);
  }

  @Override
  public int hashCode() {
    return val.hashCode();
  }

  @Override
  public String toString() {
    return val.toString();
  }

}
