package ca.uwaterloo.shediac.constant;

public class IntConstant implements Constant {

  private Integer val;

  public IntConstant(final int n) {
    val = n;
  }

  @Override
  public int compareTo(final Constant obj) {
    final IntConstant k = (IntConstant) obj;
    return val.compareTo(k.val);
  }

  @Override
  public Object asJavaVal() {
    return val;
  }

  @Override
  public boolean equals(final Object obj) {
    IntConstant k = (IntConstant) obj;
    return k != null && val.equals(k.val);
  }

  @Override
  public int hashCode() {
    return val.hashCode();
  }

  @Override
  public String toString() {
    return val.toString();
  }

}
