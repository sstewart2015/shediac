package ca.uwaterloo.shediac.constant;

public class FloatConstant implements Constant {

  private Float val;

  public FloatConstant(final float n) {
    val = n;
  }

  @Override
  public int compareTo(final Constant obj) {
    final FloatConstant k = (FloatConstant) obj;
    return val.compareTo(k.val);
  }

  @Override
  public Object asJavaVal() {
    return val;
  }

  @Override
  public boolean equals(final Object obj) {
    FloatConstant k = (FloatConstant) obj;
    return k != null && val.equals(k.val);
  }

  @Override
  public int hashCode() {
    return val.hashCode();
  }

  @Override
  public String toString() {
    return val.toString();
  }

}
