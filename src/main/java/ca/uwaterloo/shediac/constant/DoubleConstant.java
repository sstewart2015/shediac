package ca.uwaterloo.shediac.constant;

public class DoubleConstant implements Constant {

  private Double val;

  public DoubleConstant(final double n) {
    val = n;
  }

  @Override
  public int compareTo(final Constant obj) {
    final DoubleConstant k = (DoubleConstant) obj;
    return val.compareTo(k.val);
  }

  @Override
  public Object asJavaVal() {
    return val;
  }

  @Override
  public boolean equals(final Object obj) {
    DoubleConstant k = (DoubleConstant) obj;
    return k != null && val.equals(k.val);
  }

  @Override
  public int hashCode() {
    return val.hashCode();
  }

  @Override
  public String toString() {
    return val.toString();
  }

}
