package ca.uwaterloo.shediac.constant;

public class ByteConstant implements Constant {

  private Byte val;

  public ByteConstant(final byte n) {
    val = n;
  }

  @Override
  public int compareTo(final Constant obj) {
    final ByteConstant k = (ByteConstant) obj;
    return val.compareTo(k.val);
  }

  @Override
  public Object asJavaVal() {
    return val;
  }

  @Override
  public boolean equals(final Object obj) {
    ByteConstant k = (ByteConstant) obj;
    return k != null && val.equals(k.val);
  }

  @Override
  public int hashCode() {
    return val.hashCode();
  }

  @Override
  public String toString() {
    return val.toString();
  }

}
