package ca.uwaterloo.shediac.constant;

public enum Type {
  Integer, Double, Float, Long, Short, Byte, Boolean, String
}
