package ca.uwaterloo.shediac.constant;

public class ShortConstant implements Constant {

  private Short val;

  public ShortConstant(final short n) {
    val = n;
  }

  @Override
  public int compareTo(final Constant obj) {
    final ShortConstant k = (ShortConstant) obj;
    return val.compareTo(k.val);
  }

  @Override
  public Object asJavaVal() {
    return val;
  }

  @Override
  public boolean equals(final Object obj) {
    ShortConstant k = (ShortConstant) obj;
    return k != null && val.equals(k.val);
  }

  @Override
  public int hashCode() {
    return val.hashCode();
  }

  @Override
  public String toString() {
    return val.toString();
  }

}
