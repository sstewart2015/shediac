package ca.uwaterloo.shediac;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Queue;
import ca.uwaterloo.shediac.memory.Buffer;
import ca.uwaterloo.shediac.memory.Buffer.BufferAccess;
import ca.uwaterloo.shediac.memory.BufferConfig;
import ca.uwaterloo.shediac.memory.BufferHost;
import ca.uwaterloo.shediac.memory.Memory;
import ca.uwaterloo.shediac.memory.Pointer;

/**
 * This class manages the configuration and launching of kernels that may run on different kinds of
 * devices (such as CUDA or OpenCL-compatible devices).
 * 
 * @author Steven Stewart
 */
public final class KernelMgr {

  /** The available device types. */
  public enum DeviceType {
    CUDA, OpenCL
  }

  /** Maps a group ID to its respective <code>KernelGroup</code> object. */
  private final Map<Integer, KernelGroup> kernelGroups = new HashMap<>();

  /** The next group ID to be used when adding a kernel group. */
  private int groupIdx = 0;

  /** Constructs a <code>KernelMgr</code>. */
  public KernelMgr() {}

  /**
   * Runs the specified kernel with the global and local workspaces. The global workspace specifies
   * a grid of all work-items in up to 3 dimensions. The local workspace specifies how those
   * work-items are organized into a single workgroup in up to 3 dimensions.
   * 
   * @param groupId The ID of the kernel group.
   * @param global The global work-item space.
   * @param local The local work-item space.
   * @return The elapsed time in milliseconds of running the kernel.
   */
  public float runKernel(final int groupId, final int kernelId, final long[] global,
      final long[] local) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.launch(group.queue, global, local, kInfo.event);
    group.queue.finish();
    return kInfo.event.getElapsedTime();
  }

  /**
   * Releases resources allocated for the specified kernel, and removes the kernel from the kernel
   * manager's list.
   * 
   * @param groupId
   */
  public void releaseKernel(final int groupId, final int kernelId) {
    final KernelGroup group = getKernelGroup(groupId);
    group.releaseKernel(kernelId);
  }

  public int createKernelGroup(final DeviceType type, final int platformId, final int deviceId,
      boolean enableExceptions) {
    kernelGroups.put(groupIdx, KernelGroup.create(type, platformId, deviceId, enableExceptions));
    return groupIdx++;
  }

  /**
   * Returns the <code>KernelGroup</code> having the specified ID.
   * 
   * @param groupId The ID of the requested group.
   * @return The requested <code>KernelGroup</code>.
   */
  private KernelGroup getKernelGroup(final int groupId) {
    if (groupId < 0 || groupId >= kernelGroups.size())
      return null;
    return kernelGroups.get(groupId);
  }

  /**
   * Adds a new kernel for the kernel manager to manage, obtaining the kernel source from the
   * specified file.
   * 
   * @param deviceType The device type on which the kernel is intended to run.
   * @param platformId The platform ID for an available platform.
   * @param groupId The ID of the group that the is being added to.
   * @param filename The name of the source code file containing the kernel function.
   * @param name The name of the kernel function.
   * @return The kernel ID of the newly added kernel.
   */
  public int addKernel(final int groupId, final String filename, final String name) {
    // final String source = readFile(filename, Charset.defaultCharset());
    // return addKernelFromString(deviceType, platformId, deviceId, source, name);
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo =
        KernelInfo.create(group.type, group.device, group.context, name, filename);
    return group.addKernel(kInfo);
  }

  /**
   * Adds a new kernel for the kernel manager to manage, using a {@code String} for the kernel
   * source.
   * 
   * @param deviceType The device type on which the kernel is intended to run.
   * @param platformId The platform ID for an available platform.
   * @param deviceId The device ID for an available device.
   * @param source The source code containing the kernel function.
   * @param name The name of the kernel function.
   * @return The kernel ID of the newly added kernel.
   */
  public int addKernelFromString(final DeviceType deviceType, final int platformId,
      final int deviceId, final String source, final String name) {
    throw new UnsupportedOperationException();
    // final KernelInfo kInfo = KernelInfo.create(deviceType, platformId, deviceId, name, source);
    // kernels.add(kInfo);
    // return kernels.size() - 1;
  }

  /**
   * Returns a reference to the kernel metadata object for the kernel ID.
   * 
   * @param groupId The ID of the group that the kernel belongs to,
   * @param kernelId The ID of the kernel within the group.
   * @return A reference to the requested {@code KernelInfo} object.
   */
  public KernelInfo getKernelInfo(final int groupId, final int kernelId) {
    final KernelGroup group = getKernelGroup(groupId);
    return group.getKernel(kernelId);
  }

  /**
   * Call this method when adding "input/output" arguments. The host {@code data} is automatically
   * copied to a freshly allocated device buffer of equal size. A {@code Memory} object is returned
   * that has references to both the host and device buffers.
   * 
   * <p>
   * When adding an argument for "input/output", this means that the device buffer can be read from
   * (for output) and can be written to (for input) by the host.
   * </p>
   * 
   * @see Memory#copyHtoD
   * @see Memory#copyDtoH
   * @see Buffer#write
   * @see Buffer#writeAsync
   * @see Buffer#read
   * @see Buffer#readAsync
   * @param groupId The ID of the kernel group.
   * @param data A reference to the data, expecting a primitive array.
   * @return A memory object that references both host and device buffers.
   */
  public <T> Memory addArgument(final int groupId, final int kernelId, final T data) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    if (data instanceof Memory) {
      kInfo.kernel.setArgument(kInfo.argIdx++, ((Memory) data).getDeviceBuffer());
      return ((Memory) data);
    }
    final Pointer hostPtr = Pointer.to(data);
    final BufferHost hBuffer = group.device.allocateHostData(group.context, hostPtr);
    final Buffer dBuffer = group.device.allocateDevice(group.context,
        new BufferConfig(hostPtr.getSize(), BufferAccess.ReadWrite));
    dBuffer.write(group.queue, hBuffer.getSize(), hBuffer, 0L);
    kInfo.kernel.setArgument(kInfo.argIdx++, dBuffer);
    return new Memory(group.queue, dBuffer, hBuffer);
  }

  public Memory addArgumentDeviceOnly(final int groupId, final int kernelId, final int size) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    final Buffer dBuffer =
        group.device.allocateDevice(group.context, new BufferConfig(size, BufferAccess.WriteOnly));
    kInfo.kernel.setArgument(kInfo.argIdx++, dBuffer);
    return new Memory(group.queue, dBuffer, null);
  }

  /**
   * Call this method when adding an "input" argument. The host {@code data} is automatically copied
   * to a freshly allocated device buffer of equal size. A {@code Memory} object is returned that
   * has references to both the host and device buffers.
   * 
   * <p>
   * When adding an argument for "input", the device buffer can be written to by the host (i.e.,
   * providing input to the device), but it cannot be read from.
   * </p>
   * 
   * @see Memory#copyHtoD
   * @see Buffer#write
   * @see Buffer#writeAsync
   * @param groupId The ID of the kernel group.
   * @param data A reference to the data, expecting a primitive array.
   * @return A memory object that references both host and device buffers.
   */
  public <T> Memory addArgumentInput(final int groupId, final int kernelId, final T data) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    final Pointer hostPtr = Pointer.to(data);
    final BufferHost hBuffer = group.device.allocateHostData(group.context, hostPtr);
    final Buffer dBuffer = group.device.allocateDevice(group.context,
        new BufferConfig(hostPtr.getSize(), BufferAccess.WriteOnly));
    dBuffer.write(group.queue, hBuffer.getSize(), hBuffer, 0L);
    kInfo.kernel.setArgument(kInfo.argIdx++, dBuffer);
    return new Memory(group.queue, dBuffer, hBuffer);
  }

  /**
   * Call this method when adding an "output" argument. A fresh device buffer of equal size to the
   * host {@code data} is allocated, but the host {@code data} is not copied to the device. A
   * {@code Memory} object is returned that has references to both the host and device buffers.
   * 
   * <p>
   * When adding an argument for "output", the device buffer can be read from by the host (i.e.,
   * providing output from the device) to the host buffer, but it cannot be written to.
   * </p>
   * 
   * @see Memory#copyDtoH
   * @see Buffer#read
   * @see Buffer#readAsync
   * @param groupId The ID of the kernel group.
   * @param data A reference to the data, expecting a primitive array.
   * @return A memory object that references both host and device buffers.
   */
  public <T> Memory addArgumentOutput(final int groupId, final int kernelId, final T data) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    final Pointer hostPtr = Pointer.to(data);
    final BufferHost hBuffer = group.device.allocateHostData(group.context, hostPtr);
    final Buffer dBuffer = group.device.allocateDevice(group.context,
        new BufferConfig(hostPtr.getSize(), BufferAccess.ReadOnly));
    kInfo.kernel.setArgument(kInfo.argIdx++, dBuffer);
    return new Memory(group.queue, dBuffer, hBuffer);
  }

  /**
   * Call this method when adding an "output" argument. A fresh host and device buffer of equal size
   * are allocated. A {@code Memory} object is returned that has references to both the host and
   * device buffers.
   * 
   * <p>
   * When adding an argument for "output", the device buffer can be read from by the host (i.e.,
   * providing output from the device) to the host buffer, but it cannot be written to.
   * </p>
   * 
   * @see Memory#copyDtoH
   * @see Buffer#read
   * @see Buffer#readAsync
   * @param groupId The ID of the kernel group.
   * @param data A reference to the data, expecting a primitive array.
   * @return A memory object that references both host and device buffers.
   */
  public <T> Memory addArgumentOutput(final int groupId, final int kernelId, final int size) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    final BufferHost hBuffer = group.device.allocateHost(group.context, size);
    final Buffer dBuffer =
        group.device.allocateDevice(group.context, new BufferConfig(size, BufferAccess.ReadOnly));
    kInfo.kernel.setArgument(kInfo.argIdx++, dBuffer);
    return new Memory(group.queue, dBuffer, hBuffer);
  }

  /**
   * Adds an integer value as the next argument of the specified kernel.
   * 
   * @param groupId The ID of the kernel group.
   * @param val The {@code int} argument to be added.
   */
  public void addArgumentScalar(final int groupId, final int kernelId, final int val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.setArgument(kInfo.argIdx++, val);
  }

  /**
   * Adds a {@code float} value as the next argument of the specified kernel.
   * 
   * @param groupId The ID of the kernel group.
   * @param val The {@code float} argument to be added.
   */
  public void addArgumentScalar(final int groupId, final int kernelId, final float val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.setArgument(kInfo.argIdx++, val);
  }


  /**
   * Adds a {@code double} value as the next argument of the specified kernel.
   * 
   * @param groupId The ID of the kernel group.
   * @param val The {@code double} argument to be added.
   */
  public void addArgumentScalar(final int groupId, final int kernelId, final double val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.setArgument(kInfo.argIdx++, val);
  }

  /**
   * Adds a {@code long} value as the next argument of the specified kernel.
   * 
   * @param groupId The ID of the kernel group.
   * @param val The {@code long} argument to be added.
   */
  public void addArgumentScalar(final int groupId, final int kernelId, final long val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.setArgument(kInfo.argIdx++, val);
  }

  /**
   * Adds a {@code short} value as the next argument of the specified kernel.
   * 
   * @param groupId The ID of the kernel group.
   * @param val The {@code short} argument to be added.
   */
  public void addArgumentScalar(final int groupId, final int kernelId, final short val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.setArgument(kInfo.argIdx++, val);
  }

  /**
   * Adds a {@code char} value as the next argument of the specified kernel.
   * 
   * @param groupId The ID of the kernel group.
   * @param val The {@code char} argument to be added.
   */
  public void addArgumentScalar(final int groupId, final int kernelId, final char val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.setArgument(kInfo.argIdx++, val);
  }

  /**
   * Adds a {@code byte} value as the next argument of the specified kernel.
   * 
   * @param groupId The ID of the kernel group.
   * @param val The {@code char} argument to be added.
   */
  public void addArgumentScalar(final int groupId, final int kernelId, final byte val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.setArgument(kInfo.argIdx++, val);
  }

  public void setArgumentScalar(final int groupId, final int kernelId, final int argIdx, final int val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    if (argIdx >= kInfo.argIdx)
      throw new IllegalArgumentException();
    kInfo.kernel.setArgument(argIdx, val);
  }
  
  public void setArgumentScalar(final int groupId, final int kernelId, final int argIdx, final float val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    if (argIdx >= kInfo.argIdx)
      throw new IllegalArgumentException();
    kInfo.kernel.setArgument(argIdx, val);
  }
  
  public void setArgument(final int groupId, final int kernelId, final int argIdx, Buffer val) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    if (argIdx >= kInfo.argIdx)
      throw new IllegalArgumentException();
    kInfo.kernel.setArgument(argIdx, val);
  }
  
  /**
   * Adds the next argument to the kernel, allocating {@code localBytes} bytes of OpenCL local
   * memory (CUDA shared memory) per workgroup.
   * 
   * @param groupId The ID of the kernel group.
   * @param localBytes The number of bytes of local/shared memory to allocate per workgroup.
   */
  public void addArgumentLocal(final int groupId, final int kernelId, final int localBytes) {
    final KernelGroup group = getKernelGroup(groupId);
    final KernelInfo kInfo = group.getKernel(kernelId);
    kInfo.kernel.setArgumentLocal(kInfo.argIdx++, localBytes);
  }

  /**
   * This methods returns a string containing the content of a text file whose location is specified
   * by path.
   *
   * @param path The location of the file.
   * @param encoding The character encoding of the file.
   * @return A String containing the contents of the file.
   * @throws IOException
   */
  public static String readFile(final String path, final Charset encoding) throws IOException {
    final byte[] encoded = Files.readAllBytes(Paths.get(path));
    return new String(encoded, encoding);
  }

  /**
   * A reference to the {@code Queue} (or stream) for the specified kernel group.
   * 
   * @param groupId The ID of the kernel group.
   * @return A reference to the device command queue for the kernel group.
   */
  public Queue getQueue(final int groupId) {
    return getKernelGroup(groupId).queue;
  }

  /**
   * A reference to the {@code Device} for the specified kernel group.
   * 
   * @param groupId The ID of the kernel group.
   * @return A reference to the device.
   */
  public Device getDevice(int groupId) {
    return getKernelGroup(groupId).device;
  }

  /**
   * Runs a "warmup" kernel for the specified kernel group ID.
   */
  public static float runWarmup(final KernelMgr mgr, final int groupId) {
    final KernelGroup group = mgr.getKernelGroup(groupId);
    String filename = null;
    if (group.type == DeviceType.CUDA) {
      filename = "../shediac/kernels/CUDA/warmup/warmup.cu";
    } else if (group.type == DeviceType.OpenCL) {
      filename = "../shediac/kernels/OpenCL/warmup/warmup.cl";
    } else {
      throw new RuntimeException("Invalid device type.");
    }
    final int kernelId = mgr.addKernel(groupId, filename, "warmup");
    int[] input = {1, 2, 3, 4};
    int[] output = new int[input.length];
    mgr.addArgumentScalar(groupId, kernelId, 2);
    mgr.addArgumentScalar(groupId, kernelId, 2);
    mgr.addArgumentInput(groupId, kernelId, input);
    mgr.addArgumentOutput(groupId, kernelId, output);
    float time = mgr.runKernel(groupId, kernelId, new long[] {2, 2, 1}, new long[] {1, 1, 1});
    mgr.releaseKernel(groupId, kernelId);
    return time;
  }

  public Memory allocateDevice(final int groupId, final int size) {
    final KernelGroup group = getKernelGroup(groupId);
    final Buffer dBuffer =
        group.device.allocateDevice(group.context, new BufferConfig(size, BufferAccess.ReadWrite));
    return new Memory(group.queue, dBuffer, null);
  }

  public <T> Memory allocateDeviceFromHost(final int groupId, final T data) {
    final KernelGroup group = getKernelGroup(groupId);
    final Pointer hostPtr = Pointer.to(data);
    final BufferHost hBuffer = group.device.allocateHostData(group.context, hostPtr);
    final Buffer dBuffer = group.device.allocateDevice(group.context,
        new BufferConfig(hostPtr.getSize(), BufferAccess.ReadWrite));
    dBuffer.write(group.queue, hBuffer.getSize(), hBuffer, 0L);
    return new Memory(group.queue, dBuffer, hBuffer);
  }

}
