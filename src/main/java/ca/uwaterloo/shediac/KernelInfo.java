package ca.uwaterloo.shediac;

import java.util.ArrayList;

import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.device.Context;
import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.device.Event;
import ca.uwaterloo.shediac.device.Kernel;
import ca.uwaterloo.shediac.device.Program;
import ca.uwaterloo.shediac.device.Program.BuildStatus;
import ca.uwaterloo.shediac.device.cuda.CudaContext;
import ca.uwaterloo.shediac.device.cuda.CudaDevice;
import ca.uwaterloo.shediac.device.cuda.CudaEvent;
import ca.uwaterloo.shediac.device.cuda.CudaKernel;
import ca.uwaterloo.shediac.device.cuda.CudaProgram;
import ca.uwaterloo.shediac.device.opencl.clContext;
import ca.uwaterloo.shediac.device.opencl.clDevice;
import ca.uwaterloo.shediac.device.opencl.clEvent;
import ca.uwaterloo.shediac.device.opencl.clKernel;
import ca.uwaterloo.shediac.device.opencl.clProgram;

/**
 * 
 * 
 * @author Steven Stewart
 */
public final class KernelInfo {
  final DeviceType deviceType;

  final Event event;
  final Program program;
  final Kernel kernel;

  int argIdx = 0;

  final String name;
  final String filename;

  /** Create a KernelInfo object based on the requested device type. */
  public static KernelInfo create(final DeviceType deviceType, final Device device,
      final Context context, final String name, final String filename) {
    switch (deviceType) {
      case CUDA:
        return create_CUDA((CudaDevice) device, (CudaContext) context, name, filename);
      case OpenCL:
        return create_OpenCL((clDevice) device, (clContext) context, name, filename);
      default:
        throw new RuntimeException("Invalid or unsupported device type specified: " + deviceType);
    }
  }

  /** Returns a KernelInfo object for CUDA. */
  private static KernelInfo create_CUDA(final CudaDevice device, final CudaContext context,
      final String name, final String filename) {
    final CudaEvent e = new CudaEvent();
    final CudaProgram prog = new CudaProgram(context, filename);

    // Build the program
    BuildStatus status = prog.build(device, new ArrayList<>());
    if (!status.equals(BuildStatus.SUCCESS)) {
      throw new RuntimeException(prog.getBuildInfo(device));
    }

    final CudaKernel k = new CudaKernel(prog, name);
    return new KernelInfo(DeviceType.CUDA, e, prog, k, name, filename);
  }

  /** Returns a KernelInfo object for OpenCL. */
  private static KernelInfo create_OpenCL(final clDevice device, final clContext context,
      final String name, final String filename) {
    final clEvent e = new clEvent();
    final clProgram prog = new clProgram(context, filename);

    // Build the program
    BuildStatus status = prog.build(device, new ArrayList<>());
    if (!status.equals(BuildStatus.SUCCESS)) {
      throw new RuntimeException(prog.getBuildInfo(device));
    }

    final clKernel k = new clKernel(prog, name);
    return new KernelInfo(DeviceType.OpenCL, e, prog, k, name, filename);
  }

  private KernelInfo(final DeviceType type, final Event e, final Program prog, final Kernel k,
      final String n, final String f) {
    deviceType = type;
    event = e;
    program = prog;
    kernel = k;
    name = n;
    filename = f;
  }


}