package samples.clCudaApi;

import ca.uwaterloo.shediac.KernelMgr;
import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.memory.Memory;

public class Simple {

  final static String filenameOpenCL = "kernels/OpenCL/multiply/multiply.cl";
  final static String filenameCUDA = "kernels/CUDA/multiply/multiply.cu";

  private static DeviceType getDeviceType() {
    final String deviceTypeProperty = System.getProperty("deviceType");
    if (deviceTypeProperty != null) {
      if (deviceTypeProperty.equalsIgnoreCase("cuda"))
        return DeviceType.CUDA;
      else if (deviceTypeProperty.equalsIgnoreCase("opencl"))
        return DeviceType.OpenCL;
    }
    return DeviceType.OpenCL; // default
  }

  public static void main(String[] args) {
    final int platformId = 0;
    final int deviceId = 0;
    final int size = 256 * 256;
    final int multiplyFactor = 2;
    String filename;

    // Check device type and set filename accordingly
    final DeviceType type = getDeviceType();
    if (type == DeviceType.OpenCL) {
      filename = filenameOpenCL;
    } else if (type == DeviceType.CUDA) {
      filename = filenameCUDA;
    } else {
      throw new RuntimeException("Unsupported device type requested.");
    }

    // Creates a 1D thread configuration ith thread-blocks/workgroups of 256
    // threads and a total number of threads equal to the number of elements
    // in the input/output vectors.
    final long workgroupSize = 4;
    final long[] global = {size};
    final long[] local = {workgroupSize};

    // Initializes manager and adds the kernel
    final KernelMgr mgr = new KernelMgr();
    final int groupId = mgr.createKernelGroup(type, platformId, deviceId, true);
    final int kernelId = mgr.addKernel(groupId, filename, "multiply");
    System.out.println(mgr.getDevice(kernelId).version());

    // Populates regular host vectors with example data
    final float[] x = new float[size];
    final float[] y = new float[size];
    for (int i = 0; i < size; i++) {
      x[i] = (float) i;
      y[i] = 0.0f;
    }

    // Adds the kernel arguments
    final Memory mem_x, mem_y;
    mem_x = mgr.addArgumentInput(groupId, kernelId, x);
    mem_y = mgr.addArgument(groupId, kernelId, y);
    mgr.addArgumentScalar(groupId, kernelId, multiplyFactor);

    // Run the kernel
    final float time = mgr.runKernel(groupId, kernelId, global, local);
    System.out.println(time + " ms");

    // Reads the results back to the host memory
    mem_y.copyDtoH();

    // Prints the results for a couple of indices to verify that the work
    // has been done
    System.out.println("## All done. Sampled verification:");
    final int[] verificationIndices = {4, 900};
    for (Integer idx : verificationIndices) {
      float valA = x[idx];
      float valB = y[idx];
      System.out.printf(" > %f*%d = %f\n", valA, multiplyFactor, valB);
    }

    // Releases resources
    mem_x.release();
    mem_y.release();
    mgr.releaseKernel(groupId, kernelId);

  }

}
