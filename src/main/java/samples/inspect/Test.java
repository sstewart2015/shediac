package samples.inspect;

import ca.uwaterloo.shediac.KernelMgr;
import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.memory.Memory;

public class Test {

  public void generateClause() {

  }

  public static void main(String[] args) {
    DeviceType type = DeviceType.OpenCL;
    final int platformId = 0;
    final int deviceId = 0;

    final String filenameOpenCL = "kernels/OpenCL/inspect/inspect.cl";

    final KernelMgr mgr = new KernelMgr();
    final int groupId = mgr.createKernelGroup(type, platformId, deviceId, true);
    final int kernelId = mgr.addKernel(groupId, filenameOpenCL, "inspect2");

    Device dev = mgr.getDevice(kernelId);
    System.out.println(dev.name());
    System.out.println(dev.vendor());
    System.out.println(dev.version());
    System.out.println(dev.computeUnits());

    // Input into kernel
    int n = 4;
    int max_length = 4;
    // int[] clauses = { 1, !1, !2, 1, !2, 3, !3, !4, -1, 4, 5, -1, -1, -1,
    // -1}
    int[] colclauses =
        {0b10, 0b11, 0b101, 0b10, 0b101, 0b110, 0b111, 0b1001, -1, 0b1000, 0b1010, -1, -1, -1, -1};

    // rowclauses = { 1 !2 -1 -1 !1 3 4 -1 !2 !3 5 -1 1 !4 -1 -1
    int[] rowclauses = {0b10, 0b101, -1, -1, 0b11, 0b110, 0b1000, -1, 0b101, 0b111, 0b1010, -1,
        0b10, 0b1001, -1, -1};
    int[] clauses = rowclauses;
    int[] assigns = {0, 1, -1, -1, 1};

    int[] status = new int[n];
    int[] unit = new int[1];
    int[] confl = new int[1];

    // Add kernel arguments
    final Memory mem_clauses, mem_assigns, mem_status, mem_unit, mem_confl;

    mgr.addArgumentScalar(groupId, kernelId, n);
    mgr.addArgumentScalar(groupId, kernelId, max_length);

    mem_clauses = mgr.addArgumentInput(groupId, kernelId, clauses);
    mem_assigns = mgr.addArgumentInput(groupId, kernelId, assigns);
    mem_status = mgr.addArgumentOutput(groupId, kernelId, status);
    mem_unit = mgr.addArgumentOutput(groupId, kernelId, unit);
    mem_confl = mgr.addArgumentOutput(groupId, kernelId, confl);

    // Creates a 1D thread configuration ith thread-blocks/workgroups of 256
    // threads and a total number of threads equal to the number of elements
    // in the input/output vectors.
    final int size = n;
    final long workgroupSize = 1;
    final long[] global = {size};
    final long[] local = {workgroupSize};

    // Run the kernel
    final float time = mgr.runKernel(groupId, kernelId, global, local);
    System.out.println(time + " ms");

    // Reads the results back to the host memory
    mem_status.copyDtoH();

    // Prints the final statuses
    System.out.println("Statuses:");
    for (int i = 0; i < n; i++) {
      System.out.println("Expression #: " + i + " Status: " + getStatus(status[i]));
    }

    // Release resources
    mem_clauses.release();
    mem_assigns.release();
    mem_status.release();
    mem_unit.release();
    mem_confl.release();
    mgr.releaseKernel(groupId, kernelId);
  }

  private static String getStatus(final int s) {
    if (s == -1)
      return "UNDET";
    else if (s == -2)
      return "SAT";
    else if (s == -3)
      return "CONFL";
    else {
      return "UNIT(" + ((s & 1) == 1 ? "!" : "") + (s >> 1) + ")";
    }
  }
}
