package samples.inspect;

public class CPUInspector {

  public final static int END_OF_CLAUSE = -1;

  public final static int LBOOL_FALSE = 0;
  public final static int LBOOL_TRUE = 1;
  public final int LBOOL_UNDEF = -1;

  public final static int STATUS_UNDET = -1;
  public final static int STATUS_SAT = -2;
  public final static int STATUS_CONFL = -3;

  public static int LIT_VAR(int p) {
    return (p >> 1);
  }

  public static boolean LIT_SIGN(int p) {
    return (p & 1) != 0;
  }

  public boolean LIT_NEG(int p) {
    return (p ^ 1) != 0;
  }

  public static boolean LIT_TRUE(int p, int a) {
    return ((a == LBOOL_TRUE && !LIT_SIGN(p)) || (a == LBOOL_FALSE && LIT_SIGN(p)));
  }

  public static boolean LIT_FALSE(int p, int a) {
    return ((a == LBOOL_TRUE && LIT_SIGN(p)) || (a == LBOOL_FALSE && !LIT_SIGN(p)));
  }

  public static boolean LIT_UNDET(int p, int a) {
    return (!LIT_TRUE(p, a) && !LIT_FALSE(p, a));
  }

  public static int GET_ASSIGN(int p, int[] a) {
    return (a[LIT_VAR(p) - 1]);
  }

  public static long inspect1(final int n, final int max_length, final int[] clauses,
      final int[] assigns, int[] status, int[] unit, int[] confl) {

    long now = System.nanoTime();
    // Obtain global thread index
    for (int i = 0; i < n; i++) {
      final int tid = i;

      // Check boundary
      // if (tid >= n)
      // return;

      // Initialize unit and confl
      if (tid == 0) {
        unit[0] = -1;
        confl[0] = -1;
      }

      // Variables
      boolean sat_flag = false;
      int undet = 0;
      int q = -1;

      // Inspect clause
      int idx = tid;
      int p = clauses[idx];

      while (p != END_OF_CLAUSE) {
        final int a = GET_ASSIGN(p, assigns);
        if (LIT_TRUE(p, a)) {
          sat_flag = true;
          break;
        } else if (LIT_UNDET(p, a)) {
          q = p; // saves last detected unassigned literal
          undet++;
        }
        idx += max_length;
        p = clauses[idx];
      }

      if (sat_flag) {
        // SATISFIED
        status[tid] = STATUS_SAT;
      } else if (undet == 1) {
        // UNIT
        status[tid] = q;
        unit[0] = tid;
      } else if (undet == 0) {
        // CONFLICTING
        status[tid] = STATUS_CONFL;
        confl[0] = tid;
      } else {
        // UNDETERMINED
        status[tid] = STATUS_UNDET;
      }
      // if (status[tid] == STATUS_UNDET)
      // System.out.println(tid + " status=UNDET\n");
      // else if (status[tid] == STATUS_SAT)
      // System.out.println(tid + " status=SAT\n");
      // else if (status[tid] == STATUS_CONFL)
      // System.out.println(tid + " status=CONFL\n");
      // else {
      // int u = status[tid];
      // System.out.println("status=UNIT");
      // }
    }

    return System.nanoTime() - now;

  }

  public static long inspect2(final int n, final int max_length, final int[] clauses,
      final int[] assigns, int[] status, int[] unit, int[] confl) {
    long now = System.nanoTime();
    // Obtain global thread index
    for (int i = 0; i < n; i++) {
      final int tid = i;

      // Check boundary
      // if (tid >= n)
      // return;

      // Initialize unit and confl
      if (tid == 0) {
        unit[0] = -1;
        confl[0] = -1;
      }

      int clauseIdx = tid * max_length;

      // Variables
      boolean sat_flag = false;
      int undet = 0;
      int q = -1;

      // Inspect clause
      int idx = tid;
      int p = clauses[idx];

      while (p != END_OF_CLAUSE) {
        final int a = GET_ASSIGN(p, assigns);
        if (LIT_TRUE(p, a)) {
          sat_flag = true;
          break;
        } else if (LIT_UNDET(p, a)) {
          q = p; // saves last detected unassigned literal
          undet++;
        }
        idx++;
        p = clauses[idx];
      }

      if (sat_flag) {
        // SATISFIED
        status[tid] = STATUS_SAT;
      } else if (undet == 1) {
        // UNIT
        status[tid] = q;
        unit[0] = tid;
      } else if (undet == 0) {
        // CONFLICTING
        status[tid] = STATUS_CONFL;
        confl[0] = tid;
      } else {
        // UNDETERMINED
        status[tid] = STATUS_UNDET;
      }

    }

    return System.nanoTime() - now;
  }

}
