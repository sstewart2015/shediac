package samples.inspect;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import ca.uwaterloo.shediac.KernelInfo;
import ca.uwaterloo.shediac.KernelMgr;
import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.memory.Memory;

public class TestSATQueries {

  public static void main(String[] args) {
    final boolean printToConsole = false;

    // Defaults for constants
    int numClauses = 1 << 22;
    int numVars = 1 << 10;
    int cnf = 4;
    float percentAssigned = 0.75f;
    int workgroupSize = 256;
    StorageType storageType = StorageType.RowStore;
    DeviceType devType = DeviceType.OpenCL;

    // Constants obtained from the command line
    if (args.length > 0 && (args[0].equals("help") || args.length != 7)) {
      System.err.println(
          "Usage: TestSATQueriesOpenCL <numClauses> <numVars> <cnf> <percentAssigned> <workgroupSize> <column|row> <cuda|opencl>");
      System.exit(1);
    } else if (args.length == 7) {
      numClauses = Integer.parseInt(args[0]);
      numVars = Integer.parseInt(args[1]);
      cnf = Integer.parseInt(args[2]);
      percentAssigned = Float.parseFloat(args[3]);
      workgroupSize = Integer.parseInt(args[4]);
      if (args[5].equals("column"))
        storageType = StorageType.ColumnStore;
      else if (args[5].equals("row"))
        storageType = StorageType.RowStore;
      else {
        System.err.println(
            "Usage: TestSATQueriesOpenCL <numClauses> <numVars> <cnf> <percentAssigned> <workgroupSize> <column|row> <cuda|opencl>");
        System.exit(1);
      }
      if (args[6].equals("opencl"))
        devType = DeviceType.OpenCL;
      else if (args[6].equals("cuda"))
        devType = DeviceType.CUDA;
      else {
        System.err.println(
            "Usage: TestSATQueriesOpenCL <numClauses> <numVars> <cnf> <percentAssigned> <workgroupSize> <column|row> <cuda|opencl>");
        System.exit(1);
      }
    }

    // Create a randomly-generated clause database and assignment
    int[] clauseDb = createClauseDb(cnf, numVars, numClauses, storageType);
    int[] assigns = randomAssignment(numVars, percentAssigned);

    if (printToConsole) {
      printClauses(cnf, numVars, clauseDb);
      printAssignments(assigns);
    }

    runInspector(1, numClauses, numVars, cnf, percentAssigned, workgroupSize, clauseDb, assigns,
        storageType, devType);
  }

  private static void runInspector(final int id, final int numClauses, final int numVars,
      final int cnf, final float percentAssigned, final int workgroupSize, final int[] clauseDb,
      final int[] assigns, final StorageType storageType, final DeviceType devType) {
    final DeviceType type = devType;
    final int platformId = 0;
    final int deviceId = 0;
    final String filenameOpenCL = devType == DeviceType.OpenCL ? "kernels/OpenCL/inspect/inspect.cl"
        : "kernels/CUDA/inspect/inspect.cu";

    final KernelMgr mgr = new KernelMgr();
    int fileId = storageType == StorageType.ColumnStore ? 1 : 2;
    final int groupId = mgr.createKernelGroup(type, platformId, deviceId, true);
    int kernelId = mgr.addKernel(groupId, filenameOpenCL, "inspect" + fileId);

    // Host memory preparations
    int[] status = new int[numClauses];
    int[] unit = new int[1];
    int[] confl = new int[1];

    // Add kernel arguments
    final Memory mem_clauses, mem_assigns, mem_status, mem_unit, mem_confl;
    KernelInfo kInfo = mgr.getKernelInfo(groupId, kernelId);

    mgr.addArgumentScalar(groupId, kernelId, numClauses);
    mgr.addArgumentScalar(groupId, kernelId, cnf);

    mem_clauses = mgr.addArgumentInput(groupId, kernelId, clauseDb);
    mem_assigns = mgr.addArgumentInput(groupId, kernelId, assigns);
    mem_status = mgr.addArgumentOutput(groupId, kernelId, status);
    mem_unit = mgr.addArgumentOutput(groupId, kernelId, unit);
    mem_confl = mgr.addArgumentOutput(groupId, kernelId, confl);

    // Creates a 1D thread configuration ith thread-blocks/workgroups of 256
    // threads and a total number of threads equal to the number of elements
    // in the input/output vectors.
    final long[] global = {numClauses, 1, 1};
    final long[] local = {workgroupSize, 1, 1};

    // Run the kernel on the device
    final float devTimeMs = mgr.runKernel(groupId, kernelId, global, local);

    // Reads the results back to the host memory
    mem_status.copyDtoH();

    // Prints the final statuses
    // System.out.println("Statuses:");
    // for (int i = 0; i < numClauses; i++) {
    // System.out.println("Expression #: " + i + " Status: "
    // + getStatus(status[i]));
    // }

    // Run the CPU version of the kernel
    double cpuTimeMs = -1;
    if (storageType == StorageType.ColumnStore) {
      cpuTimeMs =
          CPUInspector.inspect1(numClauses, cnf, clauseDb, assigns, status, unit, confl) / 1e6f;
    } else {
      cpuTimeMs =
          CPUInspector.inspect2(numClauses, cnf, clauseDb, assigns, status, unit, confl) / 1e6f;
    }

    // Compute performance metrics
    final Device dev = mgr.getDevice(kernelId);
    final double devBCPS = ((double) numClauses / devTimeMs) / 1e6f;
    final double cpuBCPS = ((double) numClauses / cpuTimeMs) / 1e6f;
    final double devBW =
        (((double) (numClauses * cnf * Integer.BYTES) / (1 << 30)) / devTimeMs) * 1000;
    final double cpuBW =
        (((double) (numClauses * cnf * Integer.BYTES) / (1 << 30)) / cpuTimeMs) * 1000;;

    // Print summary to console
    System.out
        .println("\n----Device summary----------------------------------------------------------");
    System.out.println("Platform ID                            : " + platformId);
    System.out.println("Device ID                              : " + deviceId);
    System.out.println("Vendor                                 : " + dev.vendor());
    System.out.println("Driver version                         : " + dev.version());
    System.out.println("Device type                            : " + type);
    System.out.println("Device name                            : " + dev.name());
    System.out.println("# of compute units                     : " + dev.computeUnits());

    System.out
        .println("\n----Problem setup-----------------------------------------------------------");
    System.out.println("Number of variables                    : " + numVars);
    System.out.println("Number of clauses                      : " + numClauses);
    System.out.println("Size of each clause                    : " + cnf + " CNF");
    System.out
        .println("Percentage of vars assigned            : " + (percentAssigned * 100) + " %");
    System.out.println("Clause storage type                    : "
        + (storageType == StorageType.ColumnStore ? "Column" : "Row"));

    System.out
        .println("\n----Kernel configuration----------------------------------------------------");
    System.out.printf("# of work items                        : %d\n", global[0]);
    System.out.printf("# of threads per workgroup             : %d\n", local[0]);
    System.out.printf("# of workgroups launched               : %d\n",
        (global[0] + local[0] - 1) / local[0]);

    System.out.println(
        "\n----Performance------------------------------------------------------------------");
    System.out.printf("Device time                            : %.3f ms\n", devTimeMs);
    System.out.printf("CPU time                               : %.3f ms\n", cpuTimeMs);
    System.out.printf("Relative speedup                       : %.3fx\n", (cpuTimeMs / devTimeMs));
    System.out.printf("Device rate                            : %.3f BCPS\n", devBCPS);
    System.out.printf("Device global memory bandwidth         : %.3f GB/s\n", devBW);
    System.out.printf("CPU rate                               : %.3f BCPS\n", cpuBCPS);
    System.out.printf("CPU global memory bandwidth            : %.3f GB/s\n", cpuBW);

    // Release resources
    mem_clauses.release();
    mem_assigns.release();
    mem_status.release();
    mem_unit.release();
    mem_confl.release();
    mgr.releaseKernel(groupId, kernelId);
  }

  private static String getStatus(final int s) {
    if (s == -1)
      return "UNDET";
    else if (s == -2)
      return "SAT";
    else if (s == -3)
      return "CONFL";
    else {
      return "UNIT(" + ((s & 1) == 1 ? "!" : "") + (s >> 1) + ")";
    }
  }

  // CUDA COPIED STUFF

  enum StorageType {
    ColumnStore, RowStore
  }

  private static final int LBOOL_TRUE = 1;
  private static final int LBOOL_FALSE = 0;
  private static final int LBOOL_UNDEF = -1;

  private static int[] createClauseDb(final int cnf, final int numVars, final int numClauses,
      StorageType store) {
    // Create the clause database
    int[] clauseDb = new int[numClauses * cnf];
    if (store == StorageType.ColumnStore) {
      for (int i = 0, z = 0; i < numClauses; i++, z = i) {
        final int[] c = randomClause(cnf, numVars);
        for (int j = 0; j < cnf; j++, z += cnf)
          clauseDb[z] = c[j];
      }
    } else {
      for (int i = 0, z = 0; i < numClauses; i++) {
        final int[] c = randomClause(cnf, numVars);
        for (int j = 0; j < cnf; j++, z++)
          clauseDb[z] = c[j];
      }
    }

    return clauseDb;
  }

  private static int[] randomClause(final int cnf, final int numVars) {
    final Random random = new Random();
    final int[] c = new int[cnf];
    final Set<Integer> seen = new HashSet<>();
    int count = 0;
    while (count < cnf - 1) {
      final int tmp = (random.nextInt(numVars) + 1) * 2;
      boolean polarity = random.nextFloat() < 0.5;
      if (seen.contains(tmp))
        continue;
      seen.add(tmp);
      c[count] = polarity ? tmp : tmp + 1;
      count++;
    }
    c[cnf - 1] = -1;
    return c;
  }

  private static void printClauses(final int cnf, final int numVars, final int[] clauseDb) {
    assert (clauseDb.length % cnf) == 0;
    final int numClauses = clauseDb.length / cnf;
    for (int i = 0, z = 0; i < numClauses; i++) {
      for (int j = 0; j < cnf; j++, z++) {
        int p = clauseDb[z];
        boolean psign = (p & 1) == 1;
        // int var = (p >> 1) + 1;
        int var = (p >> 1);
        System.out.printf("%s%d%s", psign ? "!" : " ", var, j + 1 < numVars ? " -- " : " -- ");
      }
      System.out.println();
    }
  }

  private static int[] randomAssignment(final int numVars, final float percentAssigned) {
    final Random random = new Random();
    final Set<Integer> seen = new HashSet<>();
    final int[] assigns = new int[numVars];

    // initialize all assignments to undefined (2)
    for (int i = 0; i < numVars; i++)
      assigns[i] = LBOOL_UNDEF; // LBOOL_UNDEF

    // Randomly set assignments
    int count = (int) (percentAssigned * numVars);
    while (count > 0) {
      int tmp = random.nextInt(numVars) + 1;
      boolean polarity = random.nextFloat() < 0.5;
      if (seen.contains(tmp))
        continue;
      seen.add(tmp);
      assigns[tmp - 1] = polarity ? LBOOL_FALSE : LBOOL_TRUE; // 0=LBOOL_TRUE,
      // 1=LBOOL_FALSE
      count--;
    }
    return assigns;
  }

  private static void printAssignments(final int[] assigns) {
    final int numVars = assigns.length;
    for (int i = 0; i < numVars; i++) {
      final String tmp;
      if (assigns[i] == -1)
        tmp = "?";
      else if (assigns[i] == 0)
        tmp = "T";
      else if (assigns[i] == 1)
        tmp = "F";
      else
        throw new RuntimeException("Invalid assignment value.");
      System.out.printf("%d=%s%s", (i + 1), tmp, i + 1 < numVars ? " " : "");
    }
    System.out.println();
  }
}
