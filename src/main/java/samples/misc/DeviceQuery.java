package samples.misc;

import ca.uwaterloo.shediac.KernelMgr;
import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.device.Device;

public class DeviceQuery {

  public static void main(String[] args) {
    final int platformId = 0;
    final int deviceId = 0;
    final KernelMgr mgr = new KernelMgr();
    final int groupId = mgr.createKernelGroup(DeviceType.OpenCL, platformId, deviceId, true);
    final Device device = mgr.getDevice(groupId);
    
    System.out.println("Platform ID              : " + platformId);
    System.out.println("Device ID                : " + deviceId);
    System.out.println("Device                   : " + device.version());
    System.out.println("Vendor                   : " + device.vendor());
    System.out.println("Name                     : " + device.name());
    System.out.println("Compute units            : " + device.computeUnits());
    System.out.println("Core clock               : " + device.coreClock());
    System.out.println("Memory bus width         : " + device.memoryBusWidth());
    System.out.println("Memory clock             : " + device.memoryClock());
    System.out.println("Memory size              : " + device.memorySize());
    System.out.println("Local nemory size        : " + device.localMemSize());
    System.out.println("Max allocation size      : " + device.maxAllocSize());
    System.out.println("Max workgroup size       : " + device.maxWorkGroupSize());
    System.out.println("Max workitem dimensions  : " + device.maxWorkItemDimensions());
    System.out.println("Capabilities             : " + device.capabilities());
  }

}
