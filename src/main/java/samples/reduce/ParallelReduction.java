package samples.reduce;

import java.util.Random;

import ca.uwaterloo.shediac.KernelMgr;
import ca.uwaterloo.shediac.KernelMgr.DeviceType;
import ca.uwaterloo.shediac.device.Device;
import ca.uwaterloo.shediac.memory.Memory;

public class ParallelReduction {

  private static int[] randomInt(final int count, final int low, final int high) {
    final Random rgen = new Random();
    final int[] result = new int[count];
    for (int i = 0; i < count; ++i) {
      final long range = (long) high - (long) low + 1L;
      final long fraction = (long) (range * rgen.nextDouble());
      result[i] = (int) (fraction + low);
    }
    return result;
  }

  private static void printArray(final int[] data, final int count) {
    for (int i = 0; i < data.length && i < count; i++)
      System.out.println("[" + i + "] " + data[i]);
  }

  private static int cpuSumReduction(final int[] data) {
    int accum = 0;
    for (int i = 0; i < data.length; i++) {
      accum += data[i];
    }
    return accum;
  }

  public static void main(String[] args) {
    final int platformId = 0;
    final int deviceId = 0;
    final String filename = "kernels/OpenCL/reduce/reduce.cl";
    final String name1 = "reduction_sum_scalar2";
    final String name2 = "reduction_sum_scalar_complete2";
    final int n = 1 << 16;
    final int low = 1;
    final int high = 10;
    final int workgroupSize = 128;
    final int localBytes = workgroupSize * Integer.BYTES;
    final int numWorkgroups = (n + workgroupSize - 1) / workgroupSize;

    // Prepare host data
    final int[] h_data = randomInt(n, low, high);
    final int[] h_result = new int[numWorkgroups];
    final int[] h_sum = new int[1];

    // Compute sum on CPU
    double cpuTime = -System.nanoTime();
    final int cpuResult = cpuSumReduction(h_data);
    cpuTime += System.nanoTime();
    cpuTime *= 1e-6f;

    // Prepare device kernel
    final KernelMgr mgr = new KernelMgr();
    final int groupId = mgr.createKernelGroup(DeviceType.OpenCL, platformId, deviceId, true);
    final int kernelId1 = mgr.addKernel(groupId, filename, name1);
    final int kernelId2 = mgr.addKernel(groupId, filename, name2);

    final Device dev = mgr.getDevice(kernelId1);
    mgr.addArgumentLocal(groupId, kernelId1, localBytes);
    final Memory mem_data = mgr.addArgumentInput(groupId, kernelId1, h_data);
    final Memory mem_result = mgr.addArgumentInput(groupId, kernelId1, h_result);

    // mgr.addArgumentLocal(kernelId2, localBytes);
    // mgr.addArgumentInput(kernelId2, mem_result.getDeviceBuffer());
    // final Memory mem_sum = mgr.addArgumentInput(kernelId2, h_sum);

    // Run kernel
    double devTime1 =
        mgr.runKernel(groupId, kernelId1, new long[] {n, 1, 1}, new long[] {workgroupSize, 1, 1});
    double devTime2 = 0;
    // mgr.runKernel(kernelId2, new long[] {numWorkgroups, 1, 1}, new long[] {workgroupSize, 1, 1});

    // Copy result from device to host
    // mem_sum.copyDtoH();

    // Complete last step of device calculation on host
    final int numGroups = (n + workgroupSize - 1) / workgroupSize;
    int devResult = h_sum[0];

    // Print summary to console
    System.out.println("--Parallel Sum Reduction--");
    System.out.printf("Device name               : %s\n", dev.name());
    System.out.printf("# of integers             : %d (from %d to %d)\n", n, low, high);
    System.out.printf("CPU result                : %d\n", cpuResult);
    System.out.printf("Device result             : %d\n", devResult);
    System.out.printf("Valid                     : %s\n", devResult == cpuResult ? "YES" : "NO");
    System.out.printf("CPU time                  : %.3f ms\n", cpuTime);
    System.out.printf("Device time               : %.3f ms\n", devTime1 + devTime2);
    System.out.printf("Speedup                   : %.3fx\n", cpuTime / (devTime1 + devTime2));


    // Free resources
    mem_data.release();
    mem_result.release();
    mgr.releaseKernel(groupId, kernelId1);
  }

}
