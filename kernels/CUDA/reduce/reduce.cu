/** Performs a sum reduction entire in global memory. */
extern "C"
__global__ void reduction_sum_scalar1(
        const int n,
        int* input,
        int* output
    )
{
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    int lid = threadIdx.x;
    int group_size = blockDim.x;

    // Boundary check
    if (tid >= n)
        return;

    // Global memory copy of original data to destination
    output[tid] = input[tid];
    __syncthreads();

    for (int i = group_size/2; i > 0; i >>= 1) {
        if (lid < i)
            output[tid] = output[tid] + output[tid + i];
        __syncthreads();
    }
}