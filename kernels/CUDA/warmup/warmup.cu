extern "C"
__global__ void warmup(const int nx, const int ny, const float* const input, float* const output) {
	const unsigned int ix = blockDim.x * blockIdx.x + threadIdx.x;
	const unsigned int iy = blockDim.y * blockIdx.y + threadIdx.y;

	if (ix < nx && iy < ny) {
		output[iy * nx + ix] = input[iy * nx + ix];
	}
}
