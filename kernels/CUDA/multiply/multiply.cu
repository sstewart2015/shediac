extern "C"
__global__ void multiply(
    const float* const x,
    float* const y,
    const int factor
) {
    const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	y[tid] = x[tid] * factor;
}
