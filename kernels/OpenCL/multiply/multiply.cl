__kernel void multiply(
    __global const float* const x,
    __global float* const y,
    const int factor
) {
    const unsigned int tid = get_global_id(0);
	y[tid] = x[tid] * factor;
}
