/**
 * OpenCL functions for inspecting clauses.
 */

#define END_OF_CLAUSE -1

typedef int lit;
typedef int var;
typedef int lbool;

#define LBOOL_FALSE 0
#define LBOOL_TRUE 1
#define LBOOL_UNDEF -1

#define STATUS_UNDET -1
#define STATUS_SAT -2
#define STATUS_CONFL -3

#define LIT_VAR(p) (p >> 1)
#define LIT_SIGN(p) (p & 1)
#define LIT_NEG(p) (p ^ 1)

#define LIT_TRUE(p, a) ((a == LBOOL_TRUE && !LIT_SIGN(p)) || (a == LBOOL_FALSE && LIT_SIGN(p)))
#define LIT_FALSE(p, a) ((a == LBOOL_TRUE && LIT_SIGN(p)) || (a == LBOOL_FALSE && !LIT_SIGN(p)))
#define LIT_UNDET(p, a) (!LIT_TRUE(p, a) && !LIT_FALSE(p, a))

#define GET_ASSIGN(p, a) (a[LIT_VAR(p) - 1])

/**
 * assumes a column-oriented layout of clauses;
 * identifies, at most, one unit clause;
 * identifies, at most, one conflicting clause
 * one-to-one mapping of threads to clauses
 */
__kernel void inspect1(
                       const unsigned int n,
                       const unsigned int max_length,
                       __global const lit* const clauses,
                       __global const lbool* const assigns,
                       __global int* const status,
                       __global int* const unit,
                       __global int* const confl
                       )
{
    // Obtain global thread index
    const int tid = get_global_id(0); // blockDim.x * blockIdx.x + threadIdx.x
    
    // Check boundary
    if (tid >= n)
        return;
    
    // Initialize unit and confl
    if (tid == 0) {
        *unit = -1;
        *confl = -1;
    }
    
    barrier(CLK_GLOBAL_MEM_FENCE);
    
    // Variables
    bool sat_flag = false;
    int undet = 0;
    lit q;
    
    // Inspect clause
    int idx = tid;
    lit p = clauses[idx];
    
    while (p != END_OF_CLAUSE) {
        const lbool a = GET_ASSIGN(p, assigns);
        if (LIT_TRUE(p, a)) {
            sat_flag = true;
            break;
        }
        else if (LIT_UNDET(p, a)) {
            q = p; // saves last detected unassigned literal
            undet++;
        }
        idx += max_length;
        p = clauses[idx];
    }
    
    barrier(CLK_GLOBAL_MEM_FENCE);
    
    if (sat_flag) {
        // SATISFIED
        status[tid] = STATUS_SAT;
    } else if (undet == 1) {
        // UNIT
        status[tid] = q;
        *unit = tid;
    } else if (undet == 0) {
        // CONFLICTING
        status[tid] = STATUS_CONFL;
        *confl = tid;
    } else {
        // UNDETERMINED
        status[tid] = STATUS_UNDET;
    }
    
  //  if (status[tid] == STATUS_UNDET)
 //   printf("[%d] status=UNDET\n", tid);
 //   else if (status[tid] == STATUS_SAT)
  //      printf("[%d] status=SAT\n", tid);
 //   else if (status[tid] == STATUS_CONFL)
 //       printf("[%d] status=CONFL\n", tid);
  //  else {
  //      int u = status[tid];
  //      printf("[%d] status=UNIT(%c%d)\n", tid, (u & 1) ? '!' : ' ', u >> 1);
  //  }

}

/**
 * assumes a row-oriented layout of clauses;
 * identifies, at most, one unit clause;
 * identifies, at most, one conflicting clause
 * one-to-one mapping of threads to clauses
 */
__kernel void inspect2(
                const unsigned int n,
                const unsigned int max_length,
                __global const lit* const clauses,
                __global const lbool* const assigns,
                __global int* const status,
                __global int* const unit,
                __global int* const confl
        )
{
    // Obtain global thread index
    const int tid = get_global_id(0); // blockDim.x * blockIdx.x + threadIdx.x

    // Check boundary
    if (tid >= n)
            return;

    // Initialize unit and confl
    if (tid == 0) {
            *unit = -1;
            *confl = -1;
    }

    int clauseIdx = tid * max_length;
    barrier(CLK_GLOBAL_MEM_FENCE);

    // Variables
    bool sat_flag = false;
    int undet = 0;
    lit q;

    // Inspect clause
    int idx = clauseIdx;
    lit p = clauses[idx];
    
    while (p != END_OF_CLAUSE) {
        const lbool a = GET_ASSIGN(p, assigns);
//        printf("lit '%s%d' is assigned '%d' => eval %c\n", (p & 1) ? "!" : "", p >> 1, a, LIT_TRUE(p, a) ? 'T' : (LIT_UNDET(p, a) ? 'U': 'F'));
        if (LIT_TRUE(p, a)) {
                sat_flag = true;
                break;
        }
        else if (LIT_UNDET(p, a)) {
                q = p; // saves last detected unassigned literal
                undet++;
        }
        idx++;
        p = clauses[idx];
    }

    barrier(CLK_GLOBAL_MEM_FENCE);

    if (sat_flag) {
            // SATISFIED
            status[tid] = STATUS_SAT;
    } else if (undet == 1) {
            // UNIT
            status[tid] = q;
            *unit = tid;
    } else if (undet == 0) {
            // CONFLICTING
            status[tid] = STATUS_CONFL;
            *confl = tid;
    } else {
            // UNDETERMINED
            status[tid] = STATUS_UNDET;
    }

  //  if (status[tid] == STATUS_UNDET)
 //   printf("[%d] status=UNDET\n", tid);
 //   else if (status[tid] == STATUS_SAT)
 //   printf("[%d] status=SAT\n", tid);
 //   else if (status[tid] == STATUS_CONFL)
 //   printf("[%d] status=CONFL\n", tid);
  //  else {
  //      int u = status[tid];
  //      printf("[%d] status=UNIT(%c%d)\n", tid, (u & 1) ? '!' : ' ', u >> 1);
  //  }
}
